const electron = require("electron");
const { app, Menu, BrowserWindow, ipcMain } = electron;
const path = require('path');
const url = require('url');
const dbSetup = require("./repository/dbSetup");
const env = require('node-env-file');
const fs = require("fs");
const electronCompile = require('electron-compile');

//import { enableLiveReload } from 'electron-compile';
//import settings from './settings';
//import ScanManager from "./services/scanManager";
//import PurchaseTicketManager from "./services/purchaseTicketManager";

//electronCompile.enableLiveReload();


const settings = require('./settings');
const ScanManager = require('./services/scanManager');
const PurchaseTicketManager = require('./services/purchaseTicketManager');


// Load any undefined ENV variables from a specified file. 
env(__dirname + '/../.env');

let settingsFolder = path.join(app.getPath("userData"), app.getName());
if (!fs.existsSync(settingsFolder)) {
    try {
        fs.mkdirSync(settingsFolder);
    } catch (err) {
        console.log(err);
    }
}

const settingsFile = path.join(settingsFolder, "settings.json");
console.log("User Data Path: " + settingsFile);

if (fs.existsSync(settingsFile)) {
    let settingsData = JSON.parse(fs.readFileSync(settingsFile));
    settings.location = settingsData.location;
    settings.allowClearUsageData = settingsData.allowClearUsageData;
    settings.appMode = settingsData.appMode;

    if (settingsData.isTestMode !== undefined) {
        settings.isTestMode = settingsData.isTestMode;
    } else if (process.env.APPLICATION_IN_TEST_MODE === true) {
        settings.isTestMode = process.env.APPLICATION_IN_TEST_MODE;
    } else {
        settings.isTestMode = false;
    }

    settings.useFullScreen = settingsData.useFullScreen === true;
} else {
    if (process.env.LOCATION) {
        settings.location = process.env.LOCATION;
    }

    if (process.env.APPLICATION_MODE) {
        settings.appMode = process.env.APPLICATION_MODE;
    }

    if (process.env.ALLOW_CLEAR_USAGE_DATA) {
        settings.allowClearUsageData = process.env.ALLOW_CLEAR_USAGE_DATA;
    }

    if (process.env.APPLICATION_IN_TEST_MODE !== undefined && process.env.APPLICATION_IN_TEST_MODE === true) {
        settings.isTestMode = true;
    } else {
        settings.isTestMode = false;
    }

    try {
        fs.writeFileSync(settingsFile, JSON.stringify(settings), { flag: "w" });
    } catch (err) {
        console.log(err);
    }
}
console.log("Test Mode: ");
console.log(settings.isTestMode);

let mainWindow = null;
const scanManager = new ScanManager(settings);
const purchaseTicketManager = new PurchaseTicketManager(settings);

app.on("ready", _ => {
    let browserWindowSettings;
    if (settings.useFullScreen === true) {
        browserWindowSettings = {
            fullscreen: true
        };
    } else {
        browserWindowSettings = {
            width: 1600,
            height: 725,
            resizable: true
        };
    }
    console.log("Test Mode - 2: ");
    console.log(browserWindowSettings);

    mainWindow = new BrowserWindow(browserWindowSettings);

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'frontEnd/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    if (settings.isTestMode === true) {
        mainWindow.webContents.openDevTools();
    }

    mainWindow.on("closed", _ => {
        mainWindow = null;
        app.quit();
    });
});

const name = electron.app.getName();
const template = [
    {
        label: "File",
        submenu: [
            {
                label: 'Preferences',
                click: _ => mainWindow.webContents.send("open-settings", settings),
                accelerator: 'Alt+P'
            },
            {
                type: "separator"
            },
            {
                label: 'Quit',
                click: _ => app.quit(),
                accelerator: 'Alt+Q'
            }
        ]
    },
    {
        label: "View",
        submenu: [
            {
                label: "Reload",
                accelerator: "CmdOrCtrl+R",
                click: _ => mainWindow.reload()
            },
            {
                label: "Open Developer Tools",
                accelerator: "CmdOrCtrl+Shift+I",
                click: _ => mainWindow.webContents.openDevTools()
            }
        ]
    },
    {
        label: "Help",
        submenu: [
            {
                label: `About ${name}`,
                click: _ => {
                    console.log('Clicked about.');
                },
                role: 'about' // OS-specific role (see Electron documentation)
            },
        ]
    }
];

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);

ipcMain.on("load-settings", event => {
    event.sender.send('settings-pushed', settings);
});

ipcMain.on("save-user-settings", (event, settingsData) => {
    settings.location = settingsData.location;
    settings.appMode = settingsData.appMode;
    settings.isTestMode = settingsData.isTestMode;
    settings.useFullScreen = settingsData.useFullScreen;
    fs.writeFile(settingsFile, JSON.stringify(settings), { flag: "w" }, err => {
        if (err) {
            console.log(err);
        } else {
            event.sender.send('settings-pushed', settings);
        }
    })
});

ipcMain.on("clear-usage-details", (event, barcode) => {
    console.log("Barcode for Clear: " + barcode);
    if (barcode) {
        scanManager.clearUsageDetails(barcode)
            .then(ticketViewModel => {
                event.sender.send('barcode-scanned', ticketViewModel);
            })
            .catch(err => {
                event.sender.send('barcode-scan-error', err);
            });
    }
});

ipcMain.on('submit-barcode', (event, barcode) => {
    scanManager.captureBarcodeScanForUsage(barcode)
        .then(ticketViewModel => {
            event.sender.send('barcode-scanned', ticketViewModel);
        })
        .catch(err => {
            console.log(err);
            if (err.notFound) {
                event.sender.send('barcode-scan-not-found', {barcodeId: barcode});
            } else {
                event.sender.send('barcode-scan-error', {
                    barcodeId: err.barcode,
                    message: (err.message ? err.message : (err.exception ? err.exception.message : "Unknown Error"))
                });
            }
        });
});

ipcMain.on('submit-ticket-purchase', (event, ticketPurchase) => {
    purchaseTicketManager.recordTicketPurchase(ticketPurchase)
        .then(ticketViewModel => {
            //console.log("Ticket Purchase Save Details: " + JSON.stringify(ticketViewModel));
            event.sender.send("purchase-tickets-saved", ticketViewModel);
        })
        .catch(err => {
            console.log(err);
            event.sender.send("purchase-tickets-save-error", { error: err.message });
        });
});

ipcMain.on('void-ticket-purchase', (event, ticketPurchase) => {
    purchaseTicketManager.voidTicketPurchase(ticketPurchase)
        .then(ticketViewModel => {
            //console.log("Ticket Purchase Save Details: " + JSON.stringify(ticketViewModel));
            event.sender.send("purchase-tickets-voided", ticketViewModel);
        })
        .catch(err => {
            console.log(err);
            event.sender.send("purchase-tickets-void-error", { error: err.message });
        });
});

ipcMain.on("print", _event => {
    //console.log(mainWindow.webContents.getPrinters());

    let printerName = process.env.PRINTER_NAME ? process.env.PRINTER_NAME : "POS58";
    let printSilently = process.env.PRINTER_SILENT_PRINT ? process.env.PRINTER_SILENT_PRINT : true;

    mainWindow.webContents.print({deviceName:printerName, silent: printSilently}, _ => {
        console.log('Print Successful.');
    });
    // mainWindow.webContents.printToPDF({}, (error, data) => {
    //     if (error) throw error;
    //     let pdfPath = path.join(settingsFolder, "temp.pdf");
    //     fs.writeFile(pdfPath, data, (error) => {
    //         if (error) throw error;
    //         console.log('Write PDF successfully.');
    //     });
    // });
})

console.log("ticket database Setup");
dbSetup.initialize();