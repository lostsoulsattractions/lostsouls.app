const electron = require('electron');
const ipc = electron.ipcRenderer;

let appSettings;

$("#btnSettingsTheater").on("click", _ => {
    submitLocationSettings("Theater");
});

$("#btnSettingsHospital").on("click", _ => {
    submitLocationSettings("Hospital");
});

$("#btnSettingsScan").on("click", _ => {
    submitAppModeSettings("Scan");
});

$("#btnSettingsSales").on("click", _ => {
    submitAppModeSettings("Sales");
});

$(".modal .close").on("click", _ => {
    hideSettings();
});

const submitLocationSettings = (location) => {
    appSettings.location = location;
    ipc.send('save-user-settings', appSettings);
};

const submitAppModeSettings = (appMode) => {
    appSettings.appMode = appMode;
    ipc.send('save-user-settings', appSettings);
};

const isSettingsModalVisible = _ => {
    return $(".modal").is(":visible");
};

const setupSettingsDialog = _ => {
    let allowClose = appSettings.location && appSettings.appMode;
    $(".modal .btnSettingLocation").removeClass("active");
    $(".modal .btnSettingAppMode").removeClass("active");
    $("#btnSettings" + appSettings.location).addClass("active");
    $("#btnSettings" + appSettings.appMode).addClass("active");

    if (allowClose) {
        $(".close").show();
    } else {
        $(".close").hide();
    }
};

const showSettings = () => {
    setupSettingsDialog();
    $(".modal").show();
};

const hideSettings = _ => {
    if (appSettings.location) {
        $(".modal").hide();
    }

    $("#txtScanInput").focus();
}

ipc.send("load-settings");

ipc.on('settings-pushed', (event, settings) => {
    appSettings = settings;
    console.log(appSettings);
    if (appSettings.location && appSettings.appMode) {
        if (isSettingsModalVisible()) {
            console.log(appSettings);
            setupSettingsDialog();
        }

        $("#currentFacility").html(appSettings.location);
        $("#currentAppMode").html("Ticket " + appSettings.appMode);
    } else {
        showSettings();
    }

    if (!appSettings.allowClearUsageData) {
        $("#clearUsageDetails").hide();
    }
});

ipc.on('open-settings', _ => showSettings());