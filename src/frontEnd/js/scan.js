const electron = require('electron');
const ipc = electron.ipcRenderer;
const moment = require('moment-timezone');

$("#txtScanInput").on("keyup", event => {
    if (event && event.keyCode == 13) {
        let txtScanInput = $("#txtScanInput");
        if (txtScanInput.val()) {
            ipc.send('submit-barcode', $("#txtScanInput").val());
        } else {
            barcodeError({ message: "Missing barcode scan." })
        }
    }
});

$('#btnSubmit').on('click', _ => {
    ipc.send('submit-barcode', $("#txtScanInput").val());
});

$("#clearUsageDetails").on("click", _ => {
    let barcode = $("#barcode").val();
    if (barcode){
        ipc.send("clear-usage-details", barcode);
    }
});

let timer;
$(".applicationHeader").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
    if (timer) {
        clearTimeout(timer);
    }
    setTimeout(_ => $("#container").attr("class", "container"), 3000);
 });

const barcodeError = (error) => {
    let scanErrorContainer = $("#scanError");
    scanErrorContainer.show();
    let errorMessage;
    if (typeof(error) === "string") {
        errorMessage = error;
    } else {
        errorMessage = error.message ? error.message : "Unknown Error.";
    }
    scanErrorContainer.html(`Error: ${errorMessage}`);
    $("#scanSuccess").hide();
    $("#statusIndicator").attr("class", "statusIndicator").addClass("error");
    $("#container").attr("class", "container").addClass("error");
    resetScanInput();
};

const resetScanInput = _ => {
    let scanInput = $("#txtScanInput");
    scanInput.val("");
    scanInput.focus();
};

const setScanWarningOrErrorMessage = (ticketPurchaseInfo) => {
    let usageDetailsMessage = $('#usageDetailsMessage');
    usageDetailsMessage.removeClass("warning").removeClass("error").html("");
    $('#ticketType').removeClass("error");
    if (ticketPurchaseInfo.scanSummary.status.toLowerCase() == "warning") {
        if (ticketPurchaseInfo.scanSummary.warningType == "TooManyScans") {
            usageDetailsMessage.addClass("warning");
            $("#container").attr("class", "container").addClass("warning");
            usageDetailsMessage.html("Warning: Number of Scans");
        }
        else if (ticketPurchaseInfo.scanSummary.warningType == "TimeBetweenScans") {
            usageDetailsMessage.addClass("warning");
            $("#container").attr("class", "container").addClass("warning");
            usageDetailsMessage.html("Warning: Time Between Scans");
        }
    } else if (ticketPurchaseInfo.scanSummary.status.toLowerCase() == "error"){
        usageDetailsMessage.addClass("error");
        if (ticketPurchaseInfo.scanSummary.errorType == "WrongTicketType") {
            usageDetailsMessage.html("Error: Wrong Ticket Type");
            $('#ticketType').addClass("error");
        } else if (ticketPurchaseInfo.scanSummary.errorType == "WeekdayTicketUsedOnWeekend") {
            usageDetailsMessage.html("Error: Weekday ticket used on the Weekend");
            $('#ticketType').addClass("error");
        }
    }
};

ipc.on('barcode-scanned', (evt, ticketPurchaseInfo) => {
    $("#scanError").hide();
    $("#scanSuccess").show();
    console.log(ticketPurchaseInfo);
    $("#barcode").val(ticketPurchaseInfo.barcodeId);
    $(".barcode").html(ticketPurchaseInfo.barcodeId);
    $("#statusIndicator").attr("class", "statusIndicator").addClass(ticketPurchaseInfo.scanSummary.status.toLowerCase());
    $("#container").attr("class", "container").addClass(ticketPurchaseInfo.scanSummary.status.toLowerCase());

    $('#purchaseDate').html(moment(ticketPurchaseInfo.purchaseTime).tz("America/Boise").format('LLL'));
    $('#quantity').html(ticketPurchaseInfo.quantity);
    $('#quantityDetailItem').html(ticketPurchaseInfo.quantity);
    var ticketPriceFormatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 2 });
    $('#pricePaid').html(ticketPriceFormatter.format(ticketPurchaseInfo.pricePaid));
    $('#ticketType').html(ticketPurchaseInfo.ticketType && ticketPurchaseInfo.ticketType == "TheaterAndHospital" ? "Theater &amp; Hospital" : ticketPurchaseInfo.ticketType);

    // Update Usage Details
    $('#usageDetailContainer').html("");
    if (ticketPurchaseInfo.usageDetails && ticketPurchaseInfo.usageDetails.length > 0) {
        ticketPurchaseInfo.usageDetails.forEach(element => {
            var content = document.querySelector('template').content.cloneNode(true);;
            var location = content.querySelector('.location');
            location.textContent = element.location;
            var ticketScanTime = content.querySelector('.ticketScanTime');
            ticketScanTime.textContent = moment(element.ticketScanTime).tz("America/Boise").format('LLL');
            if (ticketPurchaseInfo.scanSummary.status.toLowerCase() == "warning" && element.minutesBetweenScanThisVenue >= 3) {
                ticketScanTime.classList.add("warning");
            }

            document.querySelector('#usageDetailContainer').appendChild(
                document.importNode(content, true));
        });
    }

    setScanWarningOrErrorMessage(ticketPurchaseInfo);

    resetScanInput();
});

ipc.on('barcode-scan-not-found', (evt, data) => {
    barcodeError("Barcode not found!");
    $("#statusIndicator").attr("class", "statusIndicator").addClass("error");
    $("#container").attr("class", "container").addClass("error");
    $(".barcode").html("");
    $('#quantity').html("");
})

ipc.on('barcode-scan-error', (evt, error) => {
    console.log(error);
    barcodeError(error);
    $(".barcode").html("");
    $('#quantity').html("");
});