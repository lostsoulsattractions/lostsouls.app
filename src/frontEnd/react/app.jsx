'use strict';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Header from "./components/header.jsx";
import TicketScan from "./components/ticketScan.jsx";
import TicketSales from "./components/ticketSales.jsx";
import SettingsDialog from "./components/settingsDialog.jsx";

import store from './stores';
import { Provider } from 'react-redux';
import { loadSettings } from './actions/settingsActions';

store.dispatch(loadSettings());

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div id="container" className="container">
                    <Header />
                    <TicketScan />
                    <TicketSales />
                    <SettingsDialog />
                </div>
            </Provider>
        )
    }
}

function render() {
    var root = document.querySelector('#app');
    ReactDOM.render(<App />, root);
}

render();