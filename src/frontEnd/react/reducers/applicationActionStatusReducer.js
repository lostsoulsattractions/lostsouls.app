// Redux reducer

import * as ActionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function applicationActionStatusReducer(state = initialState.applicationActionStatus, action) {
    switch (action.type) {
        case ActionTypes.SET_APPLICATION_ACTION_STATUS:
            if (action.ticketScan) {
                return action.ticketScan;
            } else if (action.ticketPurchase) {
                return action.ticketPurchase;
            }
            return action.applicationActionStatus ? action.applicationActionStatus : "";
        default:
            return state;
    }
}