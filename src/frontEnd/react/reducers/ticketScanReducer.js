// Redux reducer

import * as ActionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function ticketScanReducer(state = initialState.ticketScan, action) {
    switch (action.type) {
        case ActionTypes.SCAN_BARCODE_RESULT:
        case ActionTypes.SCAN_BARCODE_NOT_FOUND_RESULT:
        case ActionTypes.SCAN_BARCODE_ERROR_RESULT:
            return action.ticketScan;
        default:
            return state;
    }
}