// Redux reducer

import * as ActionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function settingsReducer(state = initialState.settings, action) {
    switch (action.type) {
        case ActionTypes.LOAD_SETTINGS_SUCCESS:
            if (state.openSettingsDialog) {
                // Maintain the open state of the dialog
                action.settings.openSettingsDialog = state.openSettingsDialog
            }
            return action.settings;
        case ActionTypes.OPEN_SETTINGS_DIALOG:
        case ActionTypes.CLOSE_SETTINGS_DIALOG:
            return action.settings;
        default:
            return state;
    }
}