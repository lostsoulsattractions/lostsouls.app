'use strict';

import {combineReducers} from 'redux';
import ajaxCallsInProgress from './ajaxStatusReducer';
import settings from './settingsReducer';
import ticketScan from "./ticketScanReducer";
import ticketPurchase from "./ticketPurchaseReducer";
import applicationActionStatus from "./applicationActionStatusReducer";

const rootReducer = combineReducers({
    settings,
    ticketScan,
    ticketPurchase,
    applicationActionStatus,
    ajaxCallsInProgress
});

export default rootReducer;