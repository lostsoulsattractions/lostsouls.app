// Redux reducer

import * as ActionTypes from "../constants/actionTypes";
import initialState from "./initialState";

export default function ticketPurchaseReducer(state = initialState.ticketPurchase, action) {
    switch (action.type) {
        case ActionTypes.PURCHASE_TICKET_RESULT:
        case ActionTypes.PURCHASE_TICKET_ERROR_RESULT:
            return action.ticketPurchase;
        case ActionTypes.RESET_FOR_NEW_TICKET_PURCHASE:
            return null;
        default:
            return state;
    }
}