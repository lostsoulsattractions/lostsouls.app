// Redux reducer
'use strict';

import * as ActionTypes from "../constants/actionTypes";
import initialState from "./initialState";

function actionTypeIndicatesComplete(type) {
    if (type.substring(type.length - 8) == '_SUCCESS' ||
        type.substring(type.length - 7) == '_RESULT');
}

export default function ajaxStatusReducer(state = initialState.ajaxCallsInProgress, action) {
    if (action.type == ActionTypes.BEGIN_AJAX_CALL) {
        return state + 1;
    } else if (actionTypeIndicatesComplete(action.type)) {
        return state - 1;
    }

    return state;
}