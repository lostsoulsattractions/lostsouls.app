import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as settingsActions from "../actions/settingsActions";
import * as salesLocations from "../constants/salesLocations";

class Header extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            settings: {},
            applicationActionStatus: null
        };
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.settings 
            || this.props.settings.location !== nextProps.settings.location
            || this.props.settings.appMode !== nextProps.settings.appMode
            || this.props.settings.isTestMode !== nextProps.settings.isTestMode) {
            this.setState({ settings: nextProps.settings });
        }
        if (this.props.applicationActionStatus !== nextProps.applicationActionStatus) {
            this.setState({ applicationActionStatus: nextProps.applicationActionStatus })
        }
    }

    render() {
        const { settings } = this.state;
        let headerText;
        if (settings.location && settings.appMode) {
            headerText = "Lost Souls " + (settings.location == salesLocations.COMBINED_TICKET_OFFICE ? "Combined Ticket Office" : settings.location);
            if (settings.isTestMode) {
                headerText += " (TEST MODE)";
            }
            headerText += ": " + settings.appMode;
        } else {
            headerText = "Lost Souls Attractions";
        }
        return (
            <div className={"headerContainer " + (this.state.applicationActionStatus ? this.state.applicationActionStatus.toLowerCase() : "")}>
                <div className="applicationHeader">
                    <h1>{headerText}</h1>
                </div>
            </div>
        );

    }
}

// maps the state to the props, so they're available to this component.
function mapStateToProps(state) {
    return {
        settings: state.settings,
        applicationActionStatus: state.applicationActionStatus
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(settingsActions, dispatch)
    };
}

Header.propTypes = {
    settings: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    history: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
