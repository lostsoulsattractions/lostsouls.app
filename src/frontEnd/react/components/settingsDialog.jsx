import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as settingsActions from "../actions/settingsActions";
import * as salesLocations from '../constants/salesLocations';
import * as applicationMode from '../constants/applicationMode';

class SettingsDialog extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            settings: null
        };
        this.saveSettingsLocationTheater = this.saveSettingsLocationTheater.bind(this);
        this.saveSettingsLocationHospital = this.saveSettingsLocationHospital.bind(this);
        this.saveSettingsLocationCombinedTicketOffice = this.saveSettingsLocationCombinedTicketOffice.bind(this);
        this.saveSettingsAppModeSales = this.saveSettingsAppModeSales.bind(this);
        this.saveSettingsAppModeScan = this.saveSettingsAppModeScan.bind(this);
        this.onCloseSettingsDialog = this.onCloseSettingsDialog.bind(this);
        this.saveSetting = this.saveSetting.bind(this);
        this.canCloseDialog = this.canCloseDialog.bind(this);
        this.saveSettingsIsTestMode = this.saveSettingsIsTestMode.bind(this);
        this.saveSettingsUseFullscreen = this.saveSettingsUseFullscreen.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log("Props Received: " + JSON.stringify(nextProps));
        if (!this.props.settings || (this.props.settings && !nextProps.settings)
            || this.props.settings.location !== nextProps.settings.location
            || this.props.settings.appMode !== nextProps.settings.appMode
            || this.props.settings.isTestMode !== nextProps.settings.isTestMode
            || this.props.settings.useFullScreen !== nextProps.settings.useFullScreen
            || this.props.settings.openSettingsDialog !== nextProps.settings.openSettingsDialog) {
            this.setState({ settings: nextProps.settings });
        }
    }

    canCloseDialog() {
        // If the Location and Application Mode are set, the dialog can be closed
        return this.state.settings && this.state.settings.location && this.state.settings.appMode;
    }

    saveSetting(event, location, appMode) {
        event.preventDefault();
        let settings = Object.assign({}, this.state.settings);

        if (!location && settings.location == salesLocations.COMBINED_TICKET_OFFICE && appMode == applicationMode.SCAN) {
            // A setting of "Combined Ticket Office" and app. mode of "Scan" is not a valid combination)
            // set the location to blank.
            settings.location = null;
            settings.appMode = appMode;
        } else {
            if (location) {
                settings.location = location;
            }
            if (appMode) {
                settings.appMode = appMode;
            }
        }

        this.props.actions.saveSettings(settings);
    }

    saveSettingsLocationTheater(event) {
        this.saveSetting(event, salesLocations.THEATER, null);
    }

    saveSettingsLocationHospital(event) {
        this.saveSetting(event, salesLocations.HOSPITAL, null);
    }

    saveSettingsLocationCombinedTicketOffice(event) {
        // Location "CombinedTicketOffice" is only available when the appMode is set to "Sales"
        this.saveSetting(event, salesLocations.COMBINED_TICKET_OFFICE, applicationMode.SALES);
    }

    saveSettingsAppModeSales(event) {
        this.saveSetting(event, null, applicationMode.SALES);
    }

    saveSettingsAppModeScan(event) {
        this.saveSetting(event, null, applicationMode.SCAN);
    }

    saveSettingsIsTestMode(event) {
        event.preventDefault();
        let settings = Object.assign({}, this.state.settings);        
        settings.isTestMode = !settings.isTestMode;
        this.props.actions.saveSettings(settings);
    }

    saveSettingsUseFullscreen(event) {
        event.preventDefault();
        let settings = Object.assign({}, this.state.settings);        
        settings.useFullScreen = !settings.useFullScreen;
        this.props.actions.saveSettings(settings);
    }

    onCloseSettingsDialog(event) {
        event.preventDefault();
        if (this.canCloseDialog()) {
            let settings = Object.assign({}, this.state.settings);
            settings.openSettingsDialog = false;
            this.props.actions.closeSettingsDialog(settings);
        }
    }

    render() {
        const { settings } = this.state;

        if (!settings) {
            return (
                <div></div>
            );
        }

        let isActive = settings.openSettingsDialog || !this.canCloseDialog();

        return (
            <div id="settingsDialog" className={"modal " + (isActive ? "active" : "")}>
                <div className="modal-content">
                    {settings.location && settings.appMode &&
                        <div className="close" onClick={this.onCloseSettingsDialog}>X</div>
                    }
                    <div className="settingsContent">
                        <div className="settingsSection">
                            <div className="settingsLabel">Select a Location</div>
                            <div>
                                <button type="button" id="btnSettingsTheater" value="Theater"
                                    className={"app-btn btnSettingLocation " + (settings.location == salesLocations.THEATER ? "active" : "")}
                                    onClick={this.saveSettingsLocationTheater}>
                                    Theater
                                </button>
                                <button type="button" id="btnSettingsHospital" value="Hostpital"
                                    className={"app-btn btnSettingLocation " + (settings.location == salesLocations.HOSPITAL ? "active" : "")}
                                    onClick={this.saveSettingsLocationHospital}>
                                    Hospital
                                </button>
                                <button type="button" id="btnSettingsBoth" value="Both"
                                    className={"app-btn btnSettingLocation " + (settings.location == salesLocations.COMBINED_TICKET_OFFICE ? "active" : "")}
                                    onClick={this.saveSettingsLocationCombinedTicketOffice}>
                                    Both
                                </button>
                            </div>
                        </div>
                        <div className="settingsSection">
                            <div className="settingsLabel">Select an Application Mode</div>
                            <div>
                                <button type="button" id="btnSettingsScan" value="Scan"
                                    className={"app-btn btnSettingAppMode " + (settings.appMode == applicationMode.SCAN ? "active" : "")}
                                    onClick={this.saveSettingsAppModeScan}>
                                    Ticket Scan
                                </button>
                                <button type="button" id="btnSettingsSales" value="Sales"
                                    className={"app-btn btnSettingAppMode " + (settings.appMode == applicationMode.SALES ? "active" : "")}
                                    onClick={this.saveSettingsAppModeSales}>
                                    Ticket Sales
                                </button>
                            </div>
                        </div>
                        <div className="settingsSection">
                            <div className="settingsLabel">Other Options</div>
                            <div>
                                <button type="button" id="btnSettingsTestMode" value="Scan"
                                    className={"app-btn btnSettingTestMode " + (settings.isTestMode === true ? "active" : "")}
                                    onClick={this.saveSettingsIsTestMode}>
                                    Is In Test Mode
                                </button>
                                <button type="button" id="btnSettingsUseFullScreen" value="Sales"
                                    className={"app-btn btnSettingUseFullScreen " + (settings.useFullScreen === true ? "active" : "")}
                                    onClick={this.saveSettingsUseFullscreen}>
                                    Allow Fullscreen
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

// maps the state to the props, so they're available to this component.
function mapStateToProps(state) {
    return {
        settings: state.settings
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(settingsActions, dispatch)
    };
}

SettingsDialog.propTypes = {
    settings: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    history: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsDialog);