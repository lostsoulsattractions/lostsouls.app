import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import 'moment-timezone';

const TicketScanUsageDetails = (usageDetails, scanSummary, onClearUsageDetails) => {

    let scanSummaryMessage = "";
    let usageDetailMessageClassName = "";

    // Shouldn't need to do this. Research why an aggregate object is being created.
    if (usageDetails && usageDetails.scanSummary) {
        scanSummary = usageDetails.scanSummary;
    }
    if (usageDetails && usageDetails.onClearUsageDetails) {
        onClearUsageDetails = usageDetails.onClearUsageDetails;
    }

    if (scanSummary && scanSummary.status) {
        if (scanSummary.status.toLowerCase() == "warning") {
            usageDetailMessageClassName = "warning";
            if (scanSummary.warningType == "TooManyScans") {
                scanSummaryMessage = "Warning: Number of Scans";
            }
        } else if (scanSummary.status.toLowerCase() == "error") {
            usageDetailMessageClassName = "error";
            if (scanSummary.errorType == "VoidedTicketSale") {
                scanSummaryMessage = "Error: Voided Ticket Sale";
            } else if (scanSummary.errorType == "WrongTicketType") {
                scanSummaryMessage = "Error: Wrong Ticket Type";
            } else if (scanSummary.errorType == "BothVenuesHaveBeenUsed") {
                scanSummaryMessage = "Error: Ticket Has Already Been Used at Both Attractions. Check hand stamp.";
            } else if (scanSummary.errorType == "TooManyScans") {
                scanSummaryMessage = "Error: Number of Scans";
            } else if (scanSummary.errorType == "TimeBetweenScans") {
                scanSummaryMessage = "Error: Time Between Scans"
            }
        }
    }
    
    return (
        <div className="usageDetailsContainer">
            <div className="subHeader">Usage Details:</div>
            <div id="usageDetailContainer">
                {usageDetails && usageDetails.usageDetails && usageDetails.usageDetails.map(usageDetail => {
                    return (
                        <div key={usageDetail.ticketUsageId} className={"usageDetailRow " + (usageDetail.minutesBetweenScanThisVenue >= 2 ? "error" : "")}>
                            <span className="location">{usageDetail.location}</span>:&
                            <span className="ticketScanTime"> {usageDetail.ticketScanTime && <Moment format="l LT" tz="America/Boise">{usageDetail.ticketScanTime}</Moment>}</span>
                            {usageDetail.isSurrogateScan && <span className="surrogateScan">*</span>}
                        </div>
                    );
                })}
            </div>
            <div id="usageDetailsMessage" className={usageDetailMessageClassName}>{scanSummaryMessage}</div>
            {onClearUsageDetails && usageDetails && usageDetails.usageDetails && usageDetails.usageDetails.length > 0 &&
                <button type="button" id="clearUsageDetails" onClick={onClearUsageDetails}>Clear</button>
            }
        </div>
    );
};

TicketScanUsageDetails.propTypes = {
    usageDetails: PropTypes.array,
    scanSummary: PropTypes.object,
    onClearUsageDetails: PropTypes.func
};

export default TicketScanUsageDetails;