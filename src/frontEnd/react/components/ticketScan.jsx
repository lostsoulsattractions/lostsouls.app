import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Moment from 'react-moment';
import 'moment-timezone';
import TicketScanInput from "./ticketScanInput.jsx";
import TicketScanUsageDetails from "./ticketScanUsageDetails.jsx";
import * as ticketScanActions from "../actions/ticketScanActions";
import * as ticketTypes from '../constants/ticketTypes';

class TicketScan extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            settings: {},
            ticketScan: {},
            scannedBarcode: ""
        };
        this.ticketPriceFormatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 2 });
        this.formatCurrency = this.formatCurrency.bind(this);
        this.formatTicketType = this.formatTicketType.bind(this);
        this.onBarcodeFieldChanged = this.onBarcodeFieldChanged.bind(this);
        this.submitBarcode = this.submitBarcode.bind(this);
        this.clearUsageDetails = this.clearUsageDetails.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.settings || (this.props.settings && !nextProps.settings)
            || this.props.settings.location != nextProps.settings.location
            || this.props.settings.appMode != nextProps.settings.appMode) {
            this.setState({ settings: nextProps.settings });
        }

        if (!this.props.ticketScan) {
            this.setState({ ticketScan: nextProps.ticketScan });
        } else {
            this.setState({ ticketScan: nextProps.ticketScan, scannedBarcode: "" });
        }
    }

    formatCurrency(numberValue) {
        if (numberValue) {
            return this.ticketPriceFormatter.format(numberValue);
        }
        return this.ticketPriceFormatter.format(0);
    }

    formatTicketType(ticketType) {
        return ticketType == ticketTypes.THEATER_AND_HOSPITAL ? "Theater & Hospital" : ticketType;
    }

    onBarcodeFieldChanged(event) {
        this.setState({ scannedBarcode: event.target.value });
    }

    submitBarcode(event) {
        event.preventDefault();
        this.props.actions.scanBarcode(this.state.scannedBarcode);
    }

    clearUsageDetails(event) {
        event.preventDefault();
        if (this.state.settings && this.state.settings.allowClearUsageData
            && this.state.ticketScan && this.state.ticketScan.barcodeId) {
            this.props.actions.clearUsageDetails(this.state.ticketScan.barcodeId);
        }
    }

    render() {
        const { ticketScan, settings } = this.state;

        if (settings && settings.appMode != "Scan") {
            return null;
        }

        const scanSummary = ticketScan && ticketScan.scanSummary ? ticketScan.scanSummary : {};

        let statusClass = ticketScan && ticketScan.errorCode ? ticketScan.errorCode : scanSummary.status;
        if (statusClass) {
            statusClass = statusClass.toLowerCase();
        }

        let showOnlyErrorBox = false;
        let errorMessage;

        if (statusClass == "error") {
            if (ticketScan.errorType == "NotFound") {
                showOnlyErrorBox = true;
                errorMessage = "Barcode Not Found";
            } else if (scanSummary && scanSummary.errorType &&
                scanSummary.errorType != "WrongTicketType" && scanSummary.errorType != 'BothVenuesHaveBeenUsed' &&
                scanSummary.errorType != 'TooManyScans' && scanSummary.errorType != 'TimeBetweenScans' &&
                scanSummary.errorType != "VoidedTicketSale" && scanSummary.errorType != 'WeekdayTicketUsedOnWeekend') {
                showOnlyErrorBox = true;
                errorMessage = ticketScan && ticketScan.message ? ticketScan.message : "Unknown Error.";
            }
        }

        const usageDetails = ticketScan.usageDetails;

        return (
            <div className="ticketScanWindowContainer">
                <TicketScanInput scannedBarcode={this.state.scannedBarcode} onFieldChange={this.onBarcodeFieldChanged} onSubmitBarcode={this.submitBarcode} />

                <div className="ticketDetailContainer">
                    <div id="statusIndicator" className={"statusIndicator " + statusClass}>
                        <i className="fa fa-barcode"></i>
                        <div className="barcode">{ticketScan.barcodeId}</div>
                    </div>
                    {!showOnlyErrorBox &&
                        <div id="quantityContainer" className="quantityContainer">
                            <div>
                                <div className="subHeader">Quantity</div>
                                <div id="quantity" className="quantity">{ticketScan.quantity}</div>
                            </div>
                            <div>
                                {ticketScan.ticketTypeOriginal === ticketTypes.THEATER_AND_HOSPITAL &&
                                    <div className="stamp">Stamp</div>
                                }
                            </div>
                            {(ticketScan.ticketPriceTypeFastPass != null || ticketScan.ticketPriceTypeWeekend != null) &&
                                <div className={"ticketPriceType " + statusClass}>
                                    <div className="subHeader">Ticket Price Type</div>
                                    {ticketScan.ticketPriceTypeFastPass != null &&
                                        <div id="ticketPriceTypeFastPassGenAdmission" className="ticketPriceTypeItem">{ticketScan.ticketPriceTypeFastPass ? "Fast Pass" : "General Admission"}</div>
                                    }
                                    {ticketScan.ticketPriceTypeWeekend != null &&
                                        <div id="ticketPriceTypeWeekend" className="ticketPriceTypeItem">{ticketScan.ticketPriceTypeWeekend ? "Weekend" : "Weekday"}</div>
                                    }
                                </div>
                            }
                        </div>
                    }
                    <div id="ticketDetails" className={"ticketDetails" + (ticketScan.isTestTransaction ? " testTransaction" : "")}>
                        {showOnlyErrorBox &&
                            <div id="scanError">Error: {errorMessage}</div>
                        }
                        {!showOnlyErrorBox &&
                            <div id="scanSuccess">
                                <div className="ticketPurchaseDetailsContainer">
                                    <div className="subHeader">
                                        Details
                                        {ticketScan.isTestTransaction &&
                                            <span> (Test Transaction)</span>
                                        }
                                    </div>

                                    <div className="detailItem">
                                        Purchase Date:
                                        <span> {ticketScan.purchaseTime && <Moment format="l LT" tz="America/Boise">{ticketScan.purchaseTime}</Moment>}</span>
                                    </div>
                                    <div className="detailItem">
                                        Quantity:
                                        <span id="quantityDetailItem"> {ticketScan.quantity}</span>
                                    </div>
                                    <div className="detailItem">
                                        Price Paid:
                                        <span id="pricePaid"> {this.formatCurrency(ticketScan.pricePaid)}</span>
                                    </div>
                                    <div className="detailItem">
                                        Ticket Type:
                                        <span id="ticketType" className={scanSummary.errorType == "WrongTicketType" ? "error" : ""}> {this.formatTicketType(ticketScan.ticketType)}</span>
                                    </div>
                                    {ticketScan.ticketPriceTypeFastPass != null &&
                                        <div className="detailItem">
                                            Fast Pass:
                                            <span>{ticketScan.ticketPriceTypeFastPass ? " Yes" : " No"}</span>
                                        </div>
                                    }
                                    {ticketScan.ticketPriceTypeWeekend != null &&
                                        <div className="detailItem">
                                            Day of the Week:
                                            <span>{ticketScan.ticketPriceTypeWeekend ? " Weekend" : " Weekday"}</span>
                                        </div>
                                    }
                                    {/*
                                    <div className="detailItem">
                                        Promotion Code:
                                        <span id="promotionCodeq"> {ticketScan.promotionCodeUsed}</span>
                                    </div>
                                    <div className="detailItem">
                                        Payment Type:
                                        <span id="paymentType"> {ticketScan.paymentType}</span>
                                    </div>
                                    <div className="detailItem">
                                        Transaction Location:
                                        <span id="transactionLocation"> {ticketScan.transactionLocation}</span>
                                    </div>*/}
                                    {ticketScan.ticketType != ticketScan.ticketTypeOriginal &&
                                        <div className="detailItem">
                                            Original Ticket Type:
                                            <span id="ticketType"> {this.formatTicketType(ticketScan.ticketTypeOriginal)}</span>
                                        </div>
                                    }
                                </div>
                                <TicketScanUsageDetails usageDetails={usageDetails} scanSummary={scanSummary}
                                    onClearUsageDetails={(this.state.settings && this.state.settings.allowClearUsageData ? this.clearUsageDetails : null)} />
                            </div>
                        }
                    </div>
                </div>
            </div>
        );

    }
}

// maps the state to the props, so they're available to this component.
function mapStateToProps(state) {
    return {
        settings: state.settings,
        ticketScan: state.ticketScan
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(ticketScanActions, dispatch)
    };
}

TicketScan.propTypes = {
    settings: PropTypes.object.isRequired,
    ticketScan: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    history: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketScan);