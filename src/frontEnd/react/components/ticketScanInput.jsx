
import React from 'react';
import PropTypes from 'prop-types';

const TicketScanInput = ({ scannedBarcode, onFieldChange, onSubmitBarcode }) => {
    return (
        <form className="inputScanContainer" onSubmit={onSubmitBarcode}>
            <input type="text" id="txtScanInput" value={scannedBarcode} className="inputScan" autoFocus ref={input => input && input.focus()} onChange={onFieldChange} />
            <button type="submit" id="btnSubmit" className="btn btn-default">Scan</button>
        </form>
    );
};

TicketScanInput.propTypes = {
    scannedBarcode: PropTypes.string,
    onFieldChange: PropTypes.func.isRequired,
    onSubmitBarcode: PropTypes.func.isRequired
};

export default TicketScanInput;