import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Moment from 'react-moment';
import moment from 'moment';
import 'moment-timezone';
import * as ticketPurchaseActions from "../actions/ticketPurchaseActions";
import * as ticketTypes from '../constants/ticketTypes';
import * as ticketPrices from '../constants/ticketPrices';
import * as salesLocations from '../constants/salesLocations';
import qr from 'qr-image';

class TicketSales extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            settings: {},
            ticketPurchase: {
                ticketType: null,
                quantity: 0,
                pricePaid: 0,
                discountPricePerTicket: 0
            },
            changeDue: null
        };

        this.ticketPriceFormatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 2 });
        this.formatCurrency = this.formatCurrency.bind(this);
        this.saveTicketType = this.saveTicketType.bind(this);
        this.saveTicketTypeTheater = this.saveTicketTypeTheater.bind(this);
        this.saveTicketTypeHospital = this.saveTicketTypeHospital.bind(this);
        this.saveTicketTypeBoth = this.saveTicketTypeBoth.bind(this);
        this.saveTicketTypeGeneralAdmission = this.saveTicketTypeGeneralAdmission.bind(this);
        this.saveTicketTypeFastPass = this.saveTicketTypeFastPass.bind(this);
        this.setPaymentType = this.setPaymentType.bind(this);
        this.setPaymentTypeCash = this.setPaymentTypeCash.bind(this);
        this.setPaymentTypeCreditCard = this.setPaymentTypeCreditCard.bind(this);
        this.setPaymentTypeSplit = this.setPaymentTypeSplit.bind(this);
        this.setPaymentTypeFree = this.setPaymentTypeFree.bind(this);
        this.setPaymentTypeDiscount = this.setPaymentTypeDiscount.bind(this);
        this.increaseQuantity = this.increaseQuantity.bind(this);
        this.decreaseQuantity = this.decreaseQuantity.bind(this);

        this.increaseAmtGiven = this.increaseAmtGiven.bind(this);//CHANGE
        this.decreaseAmtGiven = this.decreaseAmtGiven.bind(this);
        this.changeAmtGiven = this.changeAmtGiven.bind(this);

        this.increaseSplitCashAmount = this.increaseSplitCashAmount.bind(this);
        this.decreaseSplitCashAmount = this.decreaseSplitCashAmount.bind(this);
        this.evaluatePrice = this.evaluatePrice.bind(this);
        this.setTicketPrice = this.setTicketPrice.bind(this);
        this.increaseDiscountPrice = this.increaseDiscountPrice.bind(this);
        this.decreaseDiscountPrice = this.decreaseDiscountPrice.bind(this);
        this.onQuantityFieldChanged = this.onQuantityFieldChanged.bind(this);
        this.onSplitCashAmountFieldChanged = this.onSplitCashAmountFieldChanged.bind(this);
        this.onDiscountPriceFieldChanged = this.onDiscountPriceFieldChanged.bind(this);
        this.saveTicketPurchase = this.saveTicketPurchase.bind(this);
        this.voidTicketSales = this.voidTicketSales.bind(this);

        // Post-sale methods
        this.buildQrImage = this.buildQrImage.bind(this);
        this.resetTicketSales = this.resetTicketSales.bind(this);
        this.print = this.print.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.settings || (this.props.settings && !nextProps.settings)
            || this.props.settings.location != nextProps.settings.location
            || this.props.settings.appMode != nextProps.settings.appMode) {
            this.setState({ settings: nextProps.settings });
        }

        if (nextProps.ticketPurchase) {
            if (nextProps.ticketPurchase.barcodeId && (nextProps.ticketPurchase.paymentType === "Split" || nextProps.ticketPurchase.paymentType === "Cash")) {
                // Ticket Purchase is complete and payment type has cash.
                let newChangeDue;
                let paymentType = nextProps.ticketPurchase.paymentType;
                if (paymentType === "Split") { 
                    newChangeDue = {
                        cashAmountDue: nextProps.ticketPurchase.splitCashAmount,
                        cashAmountGiven: nextProps.ticketPurchase.splitCashAmount
                    };
                } else { 
                    newChangeDue = {
                        cashAmountDue: nextProps.ticketPurchase.pricePaid,
                        cashAmountGiven: nextProps.ticketPurchase.pricePaid
                    };
                }

                //console.log("componentWillReceiveProps: changeDue: " + JSON.stringify(newChangeDue));
                this.setState({ ticketPurchase: nextProps.ticketPurchase, changeDue: newChangeDue });
            } else {
                //console.log("componentWillReceiveProps: Ticket Purchase Save, no setup for Change Due");
                this.setState({ ticketPurchase: nextProps.ticketPurchase });
            }
        } else if (nextProps.ticketPurchase === null) {
            // ticket purchase was reset.
            //console.log("componentWillReceiveProps: Ticket Purchase Reset");
            let resetTicketPurchase = {
                ticketType: null,
                quantity: 0,
                pricePaid: 0,
                discountPricePerTicket: 0
            };
            this.setState({ ticketPurchase: resetTicketPurchase, changeDue: null });
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.ticketPurchase && this.state.ticketPurchase.barcodeId && prevState && prevState.ticketPurchase && !prevState.ticketPurchase.barcodeId) {
            this.print();
        }
    }

    saveTicketType(selectedTicketType) {
        const currentTicketType = this.state.ticketPurchase.ticketType;

        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);

        if (!currentTicketType) {
            // Ticket type as not been assigned. Assign it to the selected type.
            ticketPurchaseUpdate.ticketType = selectedTicketType;
        } else if (currentTicketType != ticketTypes.THEATER_AND_HOSPITAL && selectedTicketType == ticketTypes.THEATER_AND_HOSPITAL) {
            // The selected ticket is not Theater & Hospital, but Theater & Hospital is being selected. Reassign the ticket type as Theater & Hospital.
            ticketPurchaseUpdate.ticketType = selectedTicketType;
        } else if (currentTicketType == ticketTypes.THEATER_AND_HOSPITAL && selectedTicketType == ticketTypes.THEATER_AND_HOSPITAL) {
            // Theater & Hospital ticket type already selected and Theater & Hospital is being deselected. Unassign it.
            ticketPurchaseUpdate.ticketType = null;
        } else if (currentTicketType == ticketTypes.THEATER_AND_HOSPITAL && selectedTicketType != ticketTypes.THEATER_AND_HOSPITAL) {
            // Theater & Hospital ticket type already selected and another type is being deselected. Reassign it to the selected type.
            ticketPurchaseUpdate.ticketType = selectedTicketType;
        } else if (this.state.settings && this.state.settings.location == salesLocations.COMBINED_TICKET_OFFICE) {
            if (currentTicketType == selectedTicketType) {
                // The selected button is clicked, so unassign a ticket type
                ticketPurchaseUpdate.ticketType = null;
            } else {
                ticketPurchaseUpdate.ticketType = selectedTicketType;
            }
        }

        this.setTicketPrice(ticketPurchaseUpdate);
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    saveTicketTypeTheater(event) {
        event.preventDefault();
        this.saveTicketType(ticketTypes.THEATER);
    }

    saveTicketTypeHospital(event) {
        event.preventDefault();
        this.saveTicketType(ticketTypes.HOSPITAL);
    }

    saveTicketTypeBoth(event) {
        event.preventDefault();
        this.saveTicketType(ticketTypes.THEATER_AND_HOSPITAL);
    }

    saveTicketTypeGeneralAdmission(event) {
        event.preventDefault();
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        ticketPurchaseUpdate.fastPass = false;
        this.setTicketPrice(ticketPurchaseUpdate);
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    saveTicketTypeFastPass(event) {
        event.preventDefault();
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        ticketPurchaseUpdate.fastPass = true;
        this.setTicketPrice(ticketPurchaseUpdate);
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    setPaymentType(paymentType) {
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        if (paymentType === "Free") {
            ticketPurchaseUpdate.paymentType = "None";
            ticketPurchaseUpdate.promotionCodeUsed = "InStore-Free";
        } else if (paymentType === "Discount") {
            // Clear the promotion code if the InStore-Discount code was previously selected
            ticketPurchaseUpdate.promotionCodeUsed = ticketPurchaseUpdate.promotionCodeUsed === "InStore-Discount" ? "" : "InStore-Discount";
        } else {
            if (paymentType == "Split") {
                if (ticketPurchaseUpdate.paymentType === "None" || this.evaluatePrice(ticketPurchaseUpdate, paymentType) > 0) {
                    // Setup the Split payment only if there's been prices all ready.
                    ticketPurchaseUpdate.paymentType = paymentType;
                    ticketPurchaseUpdate.splitCashAmount = 1;
                    ticketPurchaseUpdate.splitCreditAmount = ticketPurchaseUpdate.pricePaid - 1;
                } else {
                    ticketPurchaseUpdate.splitCashAmount = 0;
                    ticketPurchaseUpdate.splitCreditAmount = 0;
                }
            } else {
                ticketPurchaseUpdate.paymentType = paymentType;
            }

            if (ticketPurchaseUpdate.promotionCodeUsed === "InStore-Free") {
                // Clear the promotion code if it was set to FREE or DISCOUNT
                ticketPurchaseUpdate.promotionCodeUsed = "";
            }
        }

        this.setTicketPrice(ticketPurchaseUpdate);
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    setPaymentTypeCash(event) {
        event.preventDefault();
        this.setPaymentType("Cash");
    }

    setPaymentTypeCreditCard(event) {
        event.preventDefault();
        this.setPaymentType("CreditCard");
    }

    setPaymentTypeSplit(event) {
        event.preventDefault();
        this.setPaymentType("Split");
    }

    setPaymentTypeFree(event) {
        event.preventDefault();
        this.setPaymentType("Free");
    }

    setPaymentTypeDiscount(event) {
        event.preventDefault();
        this.setPaymentType("Discount");
    }

    increaseQuantity(event) {
        event.preventDefault();
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        ticketPurchaseUpdate.quantity++;
        this.setTicketPrice(ticketPurchaseUpdate);
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    decreaseQuantity(event) {
        event.preventDefault();
        if (this.state.ticketPurchase.quantity > 0) {
            let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
            ticketPurchaseUpdate.quantity--;
            this.setTicketPrice(ticketPurchaseUpdate);
            this.setState({ ticketPurchase: ticketPurchaseUpdate });
        }
    }

    increaseAmtGiven(event) {
        event.preventDefault();
        let changeDue = Object.assign({}, this.state.changeDue);
        changeDue.cashAmountGiven++;
        if(changeDue.cashAmountGiven < this.state.changeDue.cashAmountDue){
            changeDue.cashAmountGiven = this.state.changeDue.cashAmountDue;
        }
        this.setState({changeDue: changeDue});
    }

    changeAmtGiven(event) {
        let value = event.target.value;
        let changeDue = Object.assign({}, this.state.changeDue);
        if (!value) {
            changeDue.cashAmountGiven = 0;
        } else if (!isNaN(value - parseInt(value))) {
            changeDue.cashAmountGiven = parseInt(value);
        }

        this.setState({changeDue: changeDue});        
    }


    decreaseAmtGiven(event) {
        event.preventDefault();
        let changeDue = Object.assign({}, this.state.changeDue);
        changeDue.cashAmountGiven--;
        if(changeDue.cashAmountGiven < this.state.changeDue.cashAmountDue){
            changeDue.cashAmountGiven = this.state.changeDue.cashAmountDue;
        }
        this.setState({changeDue: changeDue});
    }

    increaseSplitCashAmount(event) {
        event.preventDefault();
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        this.setTicketPrice(ticketPurchaseUpdate);
        let splitCashAmount = isNaN(ticketPurchaseUpdate.splitCashAmount) ? 0 : ticketPurchaseUpdate.splitCashAmount;
        if (splitCashAmount + 1 >= ticketPurchaseUpdate.pricePaid) {
            // The full amount is cash
            ticketPurchaseUpdate.paymentType == "Cash";
        } else {
            splitCashAmount = splitCashAmount + 1;
            ticketPurchaseUpdate.splitCashAmount = splitCashAmount;
            ticketPurchaseUpdate.splitCreditAmount = ticketPurchaseUpdate.pricePaid - splitCashAmount;
            ticketPurchaseUpdate.paymentType == "Split";
        }
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    decreaseSplitCashAmount(event) {
        event.preventDefault();
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        this.setTicketPrice(ticketPurchaseUpdate);
        let splitCashAmount = isNaN(ticketPurchaseUpdate.splitCashAmount) ? 0 : ticketPurchaseUpdate.splitCashAmount;
        if (splitCashAmount - 1 <= 0) {
            // The full amount is credit
            ticketPurchaseUpdate.paymentType == "Credit";
        } else {
            splitCashAmount = splitCashAmount - 1;
            ticketPurchaseUpdate.splitCashAmount = splitCashAmount;
            ticketPurchaseUpdate.splitCreditAmount = ticketPurchaseUpdate.pricePaid - splitCashAmount;
            ticketPurchaseUpdate.paymentType == "Split";
        }
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    onQuantityFieldChanged(event) {
        let value = event.target.value;
        let newQuantity = this.state.ticketPurchase.quantity;
        if (!value) {
            newQuantity = 0;
        } else if (!isNaN(value - parseInt(value))) {
            newQuantity = parseInt(value);
        }

        if (newQuantity != this.state.ticketPurchase.quantity) {
            let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
            ticketPurchaseUpdate.quantity = newQuantity;
            this.setTicketPrice(ticketPurchaseUpdate);
            this.setState({ ticketPurchase: ticketPurchaseUpdate });
        }
    }

    onSplitCashAmountFieldChanged(event) {
        let value = event.target.value;
        let newSplitCashAmount = this.state.ticketPurchase.splitCashAmount;
        if (!value) {
            newSplitCashAmount = 0;
        } else if (!isNaN(value - parseFloat(value))) {
            newSplitCashAmount = parseFloat(value);
        }

        if (newSplitCashAmount != this.state.ticketPurchase.splitCashAmount) {
            let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
            if (ticketPurchaseUpdate.splitCashAmount >= ticketPurchaseUpdate.pricePaid) {
                // The full amount is cash
                ticketPurchaseUpdate.paymentType == "Cash";
                ticketPurchaseUpdate.splitCashAmount = 0;
                ticketPurchaseUpdate.splitCreditAmount = 0;
            } else if (ticketPurchaseUpdate.splitCashAmount <= 0) {
                // The full amount is credit
                ticketPurchaseUpdate.paymentType == "Credit";
                ticketPurchaseUpdate.splitCashAmount = 0;
                ticketPurchaseUpdate.splitCreditAmount = 0;
            } else {
                ticketPurchaseUpdate.splitCashAmount = newSplitCashAmount;
                ticketPurchaseUpdate.splitCreditAmount = ticketPurchaseUpdate.pricePaid - newSplitCashAmount;
                ticketPurchaseUpdate.paymentType == "Split";
            }

            this.setState({ ticketPurchase: ticketPurchaseUpdate });
        }
    }

    onDiscountPriceFieldChanged(event) {
        let value = event.target.value;
        let newPricePerTicket = this.state.ticketPurchase.discountPricePerTicket;
        if (!value) {
            newPricePerTicket = 0;
        } else if (!isNaN(value - parseFloat(value))) {
            newPricePerTicket = parseFloat(value);
        }

        if (newPricePerTicket != this.state.ticketPurchase.discountPricePerTicket) {
            let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
            ticketPurchaseUpdate.discountPricePerTicket = newPricePerTicket;
            this.setTicketPrice(ticketPurchaseUpdate);
            this.setState({ ticketPurchase: ticketPurchaseUpdate });
        }
    }

    increaseDiscountPrice(event) {
        event.preventDefault();
        let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
        ticketPurchaseUpdate.discountPricePerTicket++;
        this.setTicketPrice(ticketPurchaseUpdate);
        this.setState({ ticketPurchase: ticketPurchaseUpdate });
    }

    decreaseDiscountPrice(event) {
        event.preventDefault();
        if (this.state.ticketPurchase.discountPricePerTicket > 0) {
            let ticketPurchaseUpdate = Object.assign({}, this.state.ticketPurchase);
            ticketPurchaseUpdate.discountPricePerTicket--;
            this.setTicketPrice(ticketPurchaseUpdate);
            this.setState({ ticketPurchase: ticketPurchaseUpdate });
        }
    }

    evaluatePrice(ticketPurchase, desiredPaymentType) {
        let paymentTypeForEvaluate = desiredPaymentType ? desiredPaymentType : ticketPurchase.paymentType;
        if (!ticketPurchase || !ticketPurchase.quantity || !ticketPurchase.ticketType || !paymentTypeForEvaluate) {
            return 0;
        }

        if (ticketPurchase.promotionCodeUsed == "InStore-Free") {
            return 0;
        }

        if (ticketPurchase.fastPass == null || ticketPurchase.weekend == null) {
            return 0;
        }

        let perTicketPrice;
        if (ticketPurchase.promotionCodeUsed == "InStore-Discount") {
            perTicketPrice = ticketPurchase.discountPricePerTicket;
        } else if (ticketPurchase.weekend) {
            let today = moment();
            if (today >= ticketPrices.WEEKEND_LATE_SEASON_START_DATE) {
                perTicketPrice = ticketPurchase.fastPass ? ticketPrices.WEEKEND_LATE_SEASON_PER_TICKET_FAST_PASS : ticketPrices.WEEKEND_LATE_SEASON_PER_TICKET_GENERAL_ADMISSION;
            } else {
                perTicketPrice = ticketPurchase.fastPass ? ticketPrices.WEEKEND_PER_TICKET_FAST_PASS : ticketPrices.WEEKEND_PER_TICKET_GENERAL_ADMISSION;
            }
        } else {
            perTicketPrice = ticketPurchase.fastPass ? ticketPrices.WEEKDAY_PER_TICKET_FAST_PASS : ticketPrices.WEEKDAY_PER_TICKET_GENERAL_ADMISSION;
        }

        let numberOfVenues = ticketPurchase.ticketType == ticketTypes.THEATER_AND_HOSPITAL ? 2 : 1;
        let paymentFee = paymentTypeForEvaluate == "CreditCard" || paymentTypeForEvaluate == "Split" ? 1 : 0;
        return (ticketPurchase.quantity * perTicketPrice * numberOfVenues) + paymentFee;
    }

    setTicketPrice(ticketPurchase) {
        let date = moment();
        let dayOfTheWeek = date.day();
        let month = date.month();
        let dayOfTheMonth = date.date();
        if (dayOfTheWeek == 0 || dayOfTheWeek == 5 || dayOfTheWeek == 6 || (month == 9 && dayOfTheMonth == 31)) {
            // Friday or Saturday (or Sunday AM for weekends that go from 7pm to 1 am); OR Halloween (months are 0 based, so 9 == October).
            // Note: changes to this logic will also affect ticket scanning (see scanManager.js).
            ticketPurchase.weekend = true;
        } else {
            ticketPurchase.weekend = false;
        }

        ticketPurchase.pricePaid = this.evaluatePrice(ticketPurchase);
        if (ticketPurchase.paymentType == "Split") {
            if (ticketPurchase.splitCashAmount > ticketPurchase.pricePaid) {
                ticketPurchase.splitCashAmount = ticketPurchase.pricePaid - 1;
                ticketPurchase.splitCreditAmount = 1;
            } else {
                ticketPurchase.splitCreditAmount = ticketPurchase.pricePaid - ticketPurchase.splitCashAmount;
            }
        } else {
            ticketPurchase.splitCashAmount = 0;
            ticketPurchase.splitCreditAmount = 0;
        }
    }

    formatCurrency(numberValue) {
        if (numberValue || numberValue === 0) {
            return this.ticketPriceFormatter.format(numberValue);
        }
        return "";
    }

    saveTicketPurchase(event) {
        event.preventDefault();
        if (this.state.ticketPurchase.promotionCodeUsed === "InStore-Discount" && this.state.ticketPurchase.discountPricePerTicket <= 0) {
            return;
        }

        if (this.state.ticketPurchase.quantity && this.state.ticketPurchase.ticketType && this.state.ticketPurchase.fastPass != null && this.state.ticketPurchase.paymentType) {
            this.props.actions.saveTicketPurchase(this.state.ticketPurchase);
        }
    }

    voidTicketSales(event) {
        event.preventDefault();
        this.props.actions.voidTicketPurchase(this.state.ticketPurchase);
    }

    buildQrImage(barcodeId) {
        let svg_string = qr.imageSync(barcodeId, { type: 'svg' });
        return { __html: svg_string };
    }

    resetTicketSales() {
        this.props.actions.resetForNextTicketSale();
    }

    print() {
        this.props.actions.print();
    }

    render() {
        const { ticketPurchase, settings, changeDue } = this.state;
        if (settings && settings.appMode != "Sales") {
            return null;
        }

        // If a sale has occurred, show the print dialog.
        if (ticketPurchase.barcodeId) {
            return (
                <div>
                    <div className="ticketPurchaseCompleteWindowContainer">
                        <div className="appLabel">
                            <div>Ticket Type:</div>
                            <div>
                                <span>{ticketPurchase.ticketType == ticketTypes.THEATER_AND_HOSPITAL ? "Theater & Hospital" : ticketPurchase.ticketType}</span> - 
                                <span>{ticketPurchase.fastPass ? " FAST PASS" : " General Admission"}</span>
                            </div>
                            <div></div>
                        </div>
                        <div className="appLabel">
                            <div>Quantity:</div>
                            <div>{ticketPurchase.quantity}</div>
                            <div></div>
                        </div>
                        <div className="appLabel">
                            <div>Price:</div>
                            <div>{this.formatCurrency(ticketPurchase.pricePaid)}</div>
                            <div></div>
                        </div>
                        <div className="appLabel">
                            <div>Purchase Date:</div>
                            <div><Moment format="l LT" tz="America/Boise">{ticketPurchase.purchaseTime}</Moment></div>
                            <div></div>
                        </div>
                        <div className="qrCodeContainer" dangerouslySetInnerHTML={this.buildQrImage(ticketPurchase.barcodeId)}></div>
                        <div className="disclaimer">
                            Expect crawling, sliding, climbing, and limited vision. Fog machines and strobe lights in use. You are responsible for your own safety and assume all risks--including illness (COVID-19 or otherwise), serious injury, or death--inherent with this activity. Do not run. Do not touch the volunteers. Surveillance cameras are in use. Lost Souls Attractions reserves the right to deny admission or expel anyone we feel does not adhere to these rules. No refunds.
                        </div>
                        <div className="btnContainer">
                            <button className="app-btn" onClick={this.print}>Print</button>
                            <button className="app-btn" onClick={this.resetTicketSales}>Next Ticket Sale</button>
                        </div>
                        <div className="btnContainer void">
                            <button className="app-btn" onClick={this.voidTicketSales}>Void Ticket Sale</button>
                        </div>
                    </div>

                    {changeDue && changeDue.cashAmountDue &&
                        <div className="ticketPurchaseSection quantity hiddenWhenPrinted changeCalculator">
                            <h3>Change:</h3>
                            <div className="appLabel">Amount Given in cash:</div>
                            <div>
                                <div className="quantityInputs">
                                    <div>
                                        <button type="button" id="btnQuantityDecrease" className="app-btn decrease fa fa-minus" onClick={this.decreaseAmtGiven}></button>
                                    </div>
                                    <div>
                                        <input type="text" value={changeDue.cashAmountGiven} maxLength="3" onChange={this.changeAmtGiven} onClick={event => event.target.select()}></input>
                                    </div>
                                    <div>
                                        <button type="button" id="btnQuantityIncrease" className="app-btn increase fa fa-plus" onClick={this.increaseAmtGiven}></button>
                                    </div>
                                    <div></div>
                                </div>
                            </div>
                            <br/>
                            <div className="appLabel">Cash amount due: {this.formatCurrency(changeDue.cashAmountDue)}</div>
                            <br/>
                            <div className="appLabel"><b>Change: {changeDue.cashAmountGiven < changeDue.cashAmountDue?"--":this.formatCurrency(changeDue.cashAmountGiven - changeDue.cashAmountDue)}</b></div>
                        </div>
                    }

                </div>
            );
        }

        return (
            <div className="ticketPurchaseWindowContainer">
                <div className="ticketPurchaseSection ticketType">
                    <div className="appLabel">Select Ticket Type(s)</div>
                    <div className="ticketTypeInputs">
                        <div>
                            {(settings.location == salesLocations.THEATER || settings.location == salesLocations.COMBINED_TICKET_OFFICE) &&
                                <button type="button" id="btnTicketTypeTheater" value="Theater"
                                    className={"app-btn btnSalesTheater " + (ticketPurchase.ticketType == ticketTypes.THEATER ? "active" : "")}
                                    onClick={this.saveTicketTypeTheater}>
                                    Theater</button>
                            }
                            {(settings.location == salesLocations.HOSPITAL || settings.location == salesLocations.COMBINED_TICKET_OFFICE) &&
                                <button type="button" id="btnTicketTypeHospital" value="Hospital"
                                    className={"app-btn btnSalesHospital " + (ticketPurchase.ticketType == ticketTypes.HOSPITAL ? "active" : "")}
                                    onClick={this.saveTicketTypeHospital}>
                                    Hospital</button>
                            }
                            <button type="button" id="btnTicketTypeBoth" value="Split"
                                className={"app-btn btnSalesHospital " + (ticketPurchase.ticketType == ticketTypes.THEATER_AND_HOSPITAL ? "active" : "")}
                                onClick={this.saveTicketTypeBoth}>
                                Both</button>
                        </div>
                        <div>
                            <button type="button" id="btnGeneralAdmission" value="GeneralAdmission"
                                className={"app-btn btnGeneralAdmission " + (ticketPurchase.fastPass === false ? "active" : "")}
                                onClick={this.saveTicketTypeGeneralAdmission}>
                                Gen. Admission</button>
                            <button type="button" id="btnFastPass" value="FastPass"
                                className={"app-btn btnFastPass " + (ticketPurchase.fastPass === true ? "active" : "")}
                                onClick={this.saveTicketTypeFastPass}>
                                Fast Pass</button>
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
                <div className="ticketPurchaseSection quantity">
                    <div className="appLabel">Quantity:</div>
                    <div>
                        <div className="quantityInputs">
                            <div>
                                <button type="button" id="btnQuantityDecrease" className="app-btn decrease fa fa-minus" onClick={this.decreaseQuantity}></button>
                            </div>
                            <div>
                                <input type="text" value={ticketPurchase.quantity} maxLength="3" onChange={this.onQuantityFieldChanged} onClick={event => event.target.select()}></input>
                            </div>
                            <div>
                                <button type="button" id="btnQuantityIncrease" className="app-btn increase fa fa-plus" onClick={this.increaseQuantity}></button>
                            </div>
                            <div></div>
                        </div>
                    </div>
                </div>
                <div className="ticketPurchaseSection paymentType">
                    <div className="purchaseSectionGroup left">
                        <div className="appLabel">Payment Type:</div>
                        <div>
                            <button type="button" id="btnPaymentTypeCash" value="Cash"
                                className={"app-btn btnSalesTheater " + (ticketPurchase.paymentType == "Cash" ? "active" : "")}
                                onClick={this.setPaymentTypeCash}>
                                Cash</button>
                            <button type="button" id="btnPaymentTypeCreditCard" value="Credit Card"
                                className={"app-btn btnSalesTheater " + (ticketPurchase.paymentType == "CreditCard" ? "active" : "")}
                                onClick={this.setPaymentTypeCreditCard}>
                                Credit/Debit Card</button>
                            <button type="button" id="btnPaymentTypeSplit" value="Split"
                                className={"app-btn btnSalesTheater " + (ticketPurchase.paymentType == "Split" ? "active" : "")}
                                onClick={this.setPaymentTypeSplit}>
                                Split</button>
                        </div>
                        {ticketPurchase.paymentType === "Split" &&
                            <div className="splitTransaction">
                                <br/>
                                <div className="appLabel">
                                    Split Transaction:
                                </div>
                                <div>
                                    <div className="splitCashAmount">
                                        <div className="appLabel">Cash Amount:</div>
                                        <div className="splitCashAmountInputs">
                                            <div>
                                                <button type="button" id="btnSplitCashAmountDecrease" className="app-btn decrease fa fa-minus" onClick={this.decreaseSplitCashAmount}></button>
                                            </div>
                                            <div>
                                                <input type="text" value={ticketPurchase.splitCashAmount} maxLength="3" onChange={this.onSplitCashAmountFieldChanged} onClick={event => event.target.select()}></input>
                                            </div>
                                            <div>
                                                <button type="button" id="btnSplitCashAmountIncrease" className="app-btn increase fa fa-plus" onClick={this.increaseSplitCashAmount}></button>
                                            </div>
                                            <div></div>
                                        </div>
                                    </div>

                                    <div className="splitCreditAmount">
                                        <div className="appLabel">
                                            Card Amount:
                                        </div>
                                        <div className="amount">
                                            {this.formatCurrency(ticketPurchase.splitCreditAmount)}
                                        </div>
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>}
                    </div>
                    <div className="purchaseSectionGroup right">
                        <div className="appLabel">Promotion Code:</div>
                        <button type="button" id="btnPaymentTypeDiscount" value="Discount"
                            className={"app-btn btnSalesTheater " + (ticketPurchase.promotionCodeUsed == "InStore-Discount" ? "active" : "")}
                            onClick={this.setPaymentTypeDiscount}>
                            Discount</button>
                        <button type="button" id="btnPaymentTypeFree" value="Free"
                            className={"app-btn btnSalesTheater " + (ticketPurchase.promotionCodeUsed == "InStore-Free" ? "active" : "")}
                            onClick={this.setPaymentTypeFree}>
                            Free</button>
                        {ticketPurchase.promotionCodeUsed === "InStore-Discount" &&
                            <div className="ticketPurchaseSection discountPrice quantity">
                                <div className="appLabel">
                                    Price per Ticket per Attraction:
                                </div>
                                <div>
                                    <div>
                                        <button type="button" id="btnDiscountPriceDecrease" className="app-btn decrease fa fa-minus" onClick={this.decreaseDiscountPrice}></button>
                                    </div>
                                    <div>
                                        <input type="text" value={ticketPurchase.discountPricePerTicket} maxLength="3" onChange={this.onDiscountPriceFieldChanged} onClick={event => event.target.select()}></input>
                                    </div>
                                    <div>
                                        <button type="button" id="btnDiscountPriceIncrease" className="app-btn increase fa fa-plus" onClick={this.increaseDiscountPrice}></button>
                                    </div>
                                </div>
                            </div>}
                    </div>
                </div>
                <div className="clear"></div>
                <div className="ticketPurchaseSection pricePaid">
                    <div className="appLabel">Total Price: {this.formatCurrency(ticketPurchase.pricePaid)}</div>
                    <button type="button" id="btnSave" value="Save" className="app-btn" onClick={this.saveTicketPurchase}>Save</button>
                </div>
            </div >
        );
    }
}

// maps the state to the props, so they're available to this component.
function mapStateToProps(state) {
    return {
        settings: state.settings,
        ticketPurchase: state.ticketPurchase,
        changeDue: state.changeDue
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(ticketPurchaseActions, dispatch)
    };
}

TicketSales.propTypes = {
    settings: PropTypes.object.isRequired,
    ticketPurchase: PropTypes.object,
    actions: PropTypes.object.isRequired,
    history: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketSales);