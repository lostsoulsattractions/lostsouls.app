'use strict';

const electron = require('electron');
const ipc = electron.ipcRenderer;

import * as ActionTypes from "../constants/actionTypes";
import { beginAjaxCall } from './ajaxStatusActions';
import store from '../stores';

ipc.on('purchase-tickets-saved', (event, ticketPurchaseResult) => {
    store.dispatch(loadTicketPurchaseResult(ActionTypes.PURCHASE_TICKET_RESULT, ticketPurchaseResult));

    let applicationStatus = ticketPurchaseResult && ticketPurchaseResult.barcodeId && ticketPurchaseResult.id ? "OK" : "Error";
    store.dispatch(setApplicationActionStatus(applicationStatus));
});

ipc.on('purchase-tickets-save-error', (event, ticketPurchaseResult) => {
    ticketPurchaseResult.errorCode = "Error";
    store.dispatch(loadTicketPurchaseResult(ActionTypes.PURCHASE_TICKET_ERROR_RESULT, ticketPurchaseResult));
    store.dispatch(setApplicationActionStatus("Error"));
});

ipc.on('purchase-tickets-voided', (event, ticketPurchaseResult) => {
    store.dispatch(loadTicketPurchaseResult(ActionTypes.RESET_FOR_NEW_TICKET_PURCHASE))
    store.dispatch(setApplicationActionStatus(""));
});

ipc.on('purchase-tickets-void-error', (event, ticketVoidResult) => {
    ticketPurchaseResult.errorCode = "Error";
    store.dispatch(loadTicketPurchaseResult(ActionTypes.PURCHASE_TICKET_ERROR_RESULT, ticketVoidResult));
    store.dispatch(setApplicationActionStatus("Error"));
});

export function loadTicketPurchaseResult(actionType, ticketPurchaseResult) {
    return {
        type: actionType,
        ticketPurchase: ticketPurchaseResult
    }
}

export function setApplicationActionStatus(applicationActionStatus){
    return {
        type: ActionTypes.SET_APPLICATION_ACTION_STATUS,
        ticketPurchase: applicationActionStatus
    };
}

export function saveTicketPurchase(ticketPurchase) {
    return dispatch => {
        dispatch(beginAjaxCall());
        ipc.send('submit-ticket-purchase', ticketPurchase);
    };
}


export function voidTicketPurchase(ticketPurchase) {
    return dispatch => {
        dispatch(beginAjaxCall());
        ipc.send('void-ticket-purchase', ticketPurchase);
    };
}

export function print() {
    return dispatch => {
        ipc.send("print");
    }
}

export function resetForNextTicketSale() {
    return dispatch => {
        store.dispatch(loadTicketPurchaseResult(ActionTypes.RESET_FOR_NEW_TICKET_PURCHASE))
        store.dispatch(setApplicationActionStatus(""));
    }
}