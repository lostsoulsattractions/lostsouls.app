import * as ActionTypes from "../constants/actionTypes";

export function beginAjaxCall() {
    return {
        type: ActionTypes.BEGIN_AJAX_CALL
    };
}