'use strict';

const electron = require('electron');
const ipc = electron.ipcRenderer;

import * as ActionTypes from "../constants/actionTypes";
import { beginAjaxCall } from './ajaxStatusActions';
import store from '../stores';

ipc.on('settings-pushed', (event, settings) => {
    store.dispatch(loadSettingsSuccess(settings));
});

ipc.on('open-settings', (event, settings) => {
    settings.openSettingsDialog = true;
    store.dispatch({
        type: ActionTypes.OPEN_SETTINGS_DIALOG,
        settings: settings
    });
});

export function loadSettingsSuccess(settings) {
    return {
        type: ActionTypes.LOAD_SETTINGS_SUCCESS,
        settings: settings
    };
}

export function loadSettings() {
    return dispatch => {
        dispatch(beginAjaxCall());
        ipc.send("load-settings");
    };
}

export function saveSettings(settings) {
    return dispatch => {
        dispatch(beginAjaxCall());
        ipc.send('save-user-settings', settings);
    };
}

export function closeSettingsDialog(settings) {    
    return dispatch => {
        dispatch({
            type: ActionTypes.CLOSE_SETTINGS_DIALOG,
            settings: settings
        });
    }
}