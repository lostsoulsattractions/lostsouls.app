'use strict';

const electron = require('electron');
const ipc = electron.ipcRenderer;

import * as ActionTypes from "../constants/actionTypes";
import { beginAjaxCall } from './ajaxStatusActions';
import store from '../stores';

ipc.on('barcode-scanned', (event, ticketScanResult) => {
    store.dispatch(loadTicketScanResult(ActionTypes.SCAN_BARCODE_RESULT, ticketScanResult));

    let applicationStatus = ticketScanResult && ticketScanResult.scanSummary && ticketScanResult.scanSummary.status
        ? ticketScanResult.scanSummary.status
        : "Error";
    store.dispatch(setApplicationActionStatus(applicationStatus));
});

ipc.on('barcode-scan-not-found', (event, ticketScanResult) => {
    if (!ticketScanResult) {
        ticketScanResult = {};
    }

    ticketScanResult.errorCode = "Error";
    ticketScanResult.errorType = "NotFound";
    store.dispatch(loadTicketScanResult(ActionTypes.SCAN_BARCODE_NOT_FOUND_RESULT, ticketScanResult));
    store.dispatch(setApplicationActionStatus("Error"));
});

ipc.on('barcode-scan-error', (event, ticketScanResult) => {
    ticketScanResult.errorCode = "Error";
    store.dispatch(loadTicketScanResult(ActionTypes.SCAN_BARCODE_ERROR_RESULT, ticketScanResult));
    store.dispatch(setApplicationActionStatus("Error"));
});

export function loadTicketScanResult(actionType, ticketScanResult) {
    return {
        type: actionType,
        ticketScan: ticketScanResult
    };
}

export function setApplicationActionStatus(applicationActionStatus){
    return {
        type: ActionTypes.SET_APPLICATION_ACTION_STATUS,
        ticketScan: applicationActionStatus
    };
}

export function scanBarcode(barcode) {
    return dispatch => {
        dispatch(beginAjaxCall());
        ipc.send('submit-barcode', barcode);
    };
}

export function clearUsageDetails(barcodeId) {
    return dispatch => {
        dispatch(beginAjaxCall());
        ipc.send("clear-usage-details", barcodeId);
    }
}