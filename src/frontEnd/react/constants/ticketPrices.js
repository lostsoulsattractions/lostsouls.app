'use strict';
import moment from 'moment';

// Week Day
export const WEEKDAY_PER_TICKET_GENERAL_ADMISSION = 10;
export const WEEKDAY_PER_TICKET_FAST_PASS = 15;

// Weekend
export const WEEKEND_PER_TICKET_GENERAL_ADMISSION = 12;
export const WEEKEND_PER_TICKET_FAST_PASS = 22;

// Weekend Late Season
export const WEEKEND_LATE_SEASON_PER_TICKET_GENERAL_ADMISSION = 15;
export const WEEKEND_LATE_SEASON_PER_TICKET_FAST_PASS = 30;

export const WEEKEND_LATE_SEASON_START_DATE = moment("2024-10-11");