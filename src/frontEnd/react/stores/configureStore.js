// Redux store: https://redux.js.org/api-reference/store

'use strict';

import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'; // Development-only
import thunk from 'redux-thunk';

// class LostSoulsStore {
//     constructor() {
//         this.store = null;
//     }

//     getStore() {
//         return this.store;
//     }

//     configureStore(initialState) {
//         this.store =  createStore(rootReducer, initialState, applyMiddleware(thunk, reduxImmutableStateInvariant()));
//         return this.getStore();
//     };
// }

// module.exports = new LostSoulsStore();

export default function configureStore(initialState) {
    return createStore(rootReducer, initialState,
        applyMiddleware(thunk, reduxImmutableStateInvariant()));
}