import configureStore from "./configureStore";

const store = configureStore();

module.exports = store;