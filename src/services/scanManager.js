const Ticket = require('../repository/models/ticket');
const moment = require("moment");

class ScanManager {

    constructor(settings) {
        this.settings = settings;
    }

    populateViewModel(ticketModel) {
        let ticketPriceTypeComponents = ticketModel.ticketPriceType.split("-");
        let returnObject = {
            id: ticketModel._id ? String(ticketModel._id) : ticketModel.ticketPurchaseId,
            barcodeId: ticketModel.barcode ? ticketModel.barcode : ticketModel.barcodeId,
            purchaseTime: ticketModel.purchaseTime,
            quantity: ticketModel.quantity,
            pricePaid: ticketModel.pricePaid,
            ticketType: ticketModel.ticketType,
            ticketTypeOriginal: ticketModel.ticketType,
            emailAddress: ticketModel.emailAddress,
            transactionType: ticketModel.transactionType,
            transactionLocation: ticketModel.transactionLocation,
            paymentType: ticketModel.paymentType,
            isTestTransaction: ticketModel.isTestTransaction,
            promotionCodeUsed: ticketModel.promotionCodeUsed,
            isDeleted: ticketModel.isDeleted,
            stripeChargeJson: ticketModel.stripeChargeJson,
            stripeChargeId: ticketModel.stripeChargeId,
            stripeRequestId: ticketModel.stripeRequestId,
            ticketPriceType: ticketModel.ticketPriceType,
            ticketPriceTypeWeekend: ticketPriceTypeComponents.length >= 3 ? ticketPriceTypeComponents[2].toLowerCase() === "weekend" : null,
            ticketPriceTypeFastPass: ticketPriceTypeComponents.length >= 2 ? ticketPriceTypeComponents[1].toLowerCase() === "fastpass" : null,
            usageDetails: [],
            scanSummary: {
                count: {
                    thisVenue: 0,
                    otherVenue: 0
                },
                maxMinutesBetweenScan: {
                    thisVenue: null,
                    otherVenue: null
                }
            }
        };

        if (ticketModel.usageDetails) {
            let previousUsageDetailThisVenueScanTime, previousUsageDetailOtherVenueScanTime;
            for (let i = 0; i < ticketModel.usageDetails.length; i++) {
                let usageDetail = ticketModel.usageDetails[i];

                // Assign the Ticket type to the first location's scan.
                if (i == 0 && ticketModel.ticketType == "SingleAttraction") {
                    returnObject.ticketType = usageDetail.location;
                }

                let minutesBetweenScanThisVenue, minutesBetweenScanOtherVenue;
                let ticketScanTimeMoment = moment(usageDetail.ticketScanTime);
                if (usageDetail.location === this.settings.location) {
                    if (previousUsageDetailThisVenueScanTime) {
                        minutesBetweenScanThisVenue = ticketScanTimeMoment.diff(previousUsageDetailThisVenueScanTime, "minutes");
                        returnObject.scanSummary.maxMinutesBetweenScan.thisVenue = Math.max(minutesBetweenScanThisVenue, returnObject.scanSummary.maxMinutesBetweenScan.thisVenue);
                    }
                    previousUsageDetailThisVenueScanTime = ticketScanTimeMoment;
                    returnObject.scanSummary.count.thisVenue++;
                } else {
                    if (previousUsageDetailOtherVenueScanTime) {
                        minutesBetweenScanOtherVenue = ticketScanTimeMoment.diff(previousUsageDetailOtherVenueScanTime, "minutes");
                        if (!usageDetail.isSurrogateScan) {
                            returnObject.scanSummary.maxMinutesBetweenScan.otherVenue = Math.max(minutesBetweenScanOtherVenue, returnObject.scanSummary.maxMinutesBetweenScan.otherVenue);
                        }
                    }
                    previousUsageDetailOtherVenueScanTime = ticketScanTimeMoment;
                    if (!usageDetail.isSurrogateScan) {
                        returnObject.scanSummary.count.otherVenue++;
                    }
                }

                returnObject.usageDetails.push({
                    ticketUsageId: usageDetail.ticketUsageId,
                    location: usageDetail.location,
                    ticketScanTime: usageDetail.ticketScanTime,
                    isSurrogateScan: usageDetail.isSurrogateScan,
                    minutesBetweenScanThisVenue: minutesBetweenScanThisVenue,
                    minutesBetweenScanOtherVenue: minutesBetweenScanOtherVenue
                });
                console.log({
                    ticketUsageId: usageDetail.ticketUsageId,
                    location: usageDetail.location,
                    ticketScanTime: usageDetail.ticketScanTime,
                    isSurrogateScan: usageDetail.isSurrogateScan,
                    minutesBetweenScanThisVenue: minutesBetweenScanThisVenue,
                    minutesBetweenScanOtherVenue: minutesBetweenScanOtherVenue
                });
            }
        }

        if (returnObject.isDeleted) {
            console.log("Deleted ticket type");
            returnObject.scanSummary.status = "Error";
            returnObject.scanSummary.errorType = "VoidedTicketSale"
        } else if (returnObject.ticketType === "TheaterAndHospital" ||
            (this.settings.location === "Theater" && returnObject.ticketType === "Theater") ||
            (this.settings.location === "Hospital" && returnObject.ticketType === "Hospital")) {
            // TODO: Configuration drive the Scan Count and the Max allowed minutes between scans that will trigger a warning status.
            if (returnObject.ticketType === "TheaterAndHospital" && returnObject.scanSummary.count.otherVenue > 0 && returnObject.scanSummary.count.thisVenue > 1) {
                returnObject.scanSummary.status = "Error";
                returnObject.scanSummary.errorType = "BothVenuesHaveBeenUsed"
            } else if (returnObject.scanSummary.count.thisVenue > 3) {
                returnObject.scanSummary.status = "Error";
                returnObject.scanSummary.errorType = "TooManyScans";
            } else if (returnObject.scanSummary.maxMinutesBetweenScan.thisVenue && returnObject.scanSummary.maxMinutesBetweenScan.thisVenue >= 2) {
                returnObject.scanSummary.status = "Error";
                returnObject.scanSummary.errorType = "TimeBetweenScans";
            } else if (returnObject.scanSummary.count.thisVenue > 2) {
                returnObject.scanSummary.status = "Warning";
                returnObject.scanSummary.warningType = "TooManyScans";
            } else {
                let currentDayIsWeekend = this.isCurrentDayWeekendOrHalloween()
                if (currentDayIsWeekend && !returnObject.ticketPriceTypeWeekend) {
                    returnObject.scanSummary.status = "Error";
                    returnObject.scanSummary.errorType = "WeekdayTicketUsedOnWeekend";
                } else {
                    returnObject.scanSummary.status = "OK";
                }
            }
        } else {
            returnObject.scanSummary.status = "Error";
            returnObject.scanSummary.errorType = "WrongTicketType";
        }
        return returnObject;
    };

    isCurrentDayWeekendOrHalloween() {
        let date = moment();
        let dayOfTheWeek = date.day();
        let month = date.month();
        let dayOfTheMonth = date.day();
        let isWeekend;
        if (dayOfTheWeek == 0 || dayOfTheWeek == 5 || dayOfTheWeek == 6 || (month == 10 && dayOfTheMonth == 31)) {
            // Friday or Saturday (or Sunday AM for weekends that go from 7pm to 1 am); OR Halloween.
            // Note: changes to this logic will also affect ticket scanning (see scanManager.js).
            isWeekend = true;
        } else {
            isWeekend = false;
        }

        return isWeekend;
    }

    clearUsageDetails(barcode) {
        var promise = new Promise((resolve, reject) => {
            Ticket.findById(barcode)
                .then(model => {
                    if (!this.settings.allowClearUsageData) {
                        resolve(this.populateViewModel(model))
                    }
                    const ticket = new Ticket(model);
                    ticket.clearUsageData()
                        .then(updatedModel => {
                            resolve(this.populateViewModel(updatedModel));

                        })
                        .catch(err => {
                            console.log(err);
                            reject(err);
                        });
                });
        });
        return promise;
    }

    captureBarcodeScanForUsage(barcode) {
        var promise = new Promise((resolve, reject) => {
            Ticket.findById(barcode)
                .then(model => {
                    if (model) {
                        const ticket = new Ticket(model);
                        ticket.addUsageData(this.settings.location)
                            .then(updatedModel => {
                                resolve(this.populateViewModel(updatedModel));
                            })
                            .catch(err => {
                                reject({
                                    barcode: barcode,
                                    message: "Usage details could not be updated.",
                                    exception: err
                                });
                            });

                    } else {
                        reject({
                            barcode: barcode,
                            notFound: true,
                            message: `Barcode '${barcode}' not found.`
                        });
                    }
                })
                .catch(err => {
                    if (typeof err === 'string') {
                        reject({ message: err });
                    } else {
                        reject({ exception: err });
                    }
                })
        });
        return promise;
    }
}

module.exports = ScanManager;