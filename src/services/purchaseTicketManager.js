const Ticket = require('../repository/models/ticket');

class PurchaseTicketManager {

    constructor(settings) {
        this.settings = settings;
    }

    populateViewModel(ticketModel) {
        //console.log("%%%%%%%%%%%");
        //console.log("populateViewModel, Ticket Model: " + JSON.stringify(ticketModel));
        //console.log("%%%%%%%%%%%");

        let ticketPriceTypeComponents = ticketModel.ticketPriceType.split("-");
        return {
            id: ticketModel._id ? String(ticketModel._id) : ticketModel.ticketPurchaseId,
            barcodeId: ticketModel.barcode ? ticketModel.barcode : ticketModel.barcodeId,
            purchaseTime: ticketModel.purchaseTime,
            quantity: ticketModel.quantity,
            pricePaid: ticketModel.pricePaid,
            ticketType: ticketModel.ticketType,
            promotionCodeUsed: ticketModel.promotionCodeUsed,
            transactionLocation: ticketModel.transactionLocation,
            paymentType: ticketModel.paymentType,
            splitCashAmount: ticketModel.splitCashAmount,
            ticketPriceTypeWeekend: ticketPriceTypeComponents.length >= 3 ? ticketPriceTypeComponents[2].toLowerCase() === "weekend" : null,
            fastPass: ticketPriceTypeComponents.length >= 2 ? ticketPriceTypeComponents[1].toLowerCase() === "fastpass" : null,
            usageDetails: [],
            scanSummary: {
                count: {
                    thisVenue: 0,
                    otherVenue: 0
                },
                maxMinutesBetweenScan: {
                    thisVenue: null,
                    otherVenue: null
                }
            }
        };
    }

    recordTicketPurchase(ticketPurchaseInfo) {
        var promise = new Promise((resolve, reject) => {
            ticketPurchaseInfo.purchaseTime = new Date();
            ticketPurchaseInfo.barcodeId = this.uuidv4();

            ticketPurchaseInfo.transactionLocation = this.settings.location;

            if (this.settings.isTestMode) {
                ticketPurchaseInfo.isTestTransaction = true;
            }

            ticketPurchaseInfo.ticketPriceType = `${ticketPurchaseInfo.ticketType}-${ticketPurchaseInfo.fastPass ? "FastPass" : "GeneralAdmission"}-${ticketPurchaseInfo.weekend ? "Weekend" : "Weekday"}`;

            // Setup the Ticket Purchase Payment Details object.
            ticketPurchaseInfo.paymentDetails = [];
            if (ticketPurchaseInfo.paymentType == 'Split') {
                ticketPurchaseInfo.paymentDetails.push({
                    paymentType: 'Cash',
                    transactionAmount: ticketPurchaseInfo.splitCashAmount
                });
                ticketPurchaseInfo.paymentDetails.push({
                    paymentType: 'CreditCard',
                    transactionAmount: ticketPurchaseInfo.splitCreditAmount
                });
            } else if (ticketPurchaseInfo.pricePaid > 0) {
                ticketPurchaseInfo.paymentDetails.push({
                    paymentType: ticketPurchaseInfo.paymentType,
                    transactionAmount: ticketPurchaseInfo.pricePaid
                });
            }

            let newTicketPurchase = new Ticket(ticketPurchaseInfo);
            newTicketPurchase.save()
                .then(model => resolve(this.populateViewModel(model)))
                .catch(err => reject(err));
        });
        return promise;
    }

    voidTicketPurchase(ticketPurchaseInfo) {
        ticketPurchaseInfo.ticketPurchaseId = ticketPurchaseInfo.id;
        var promise = new Promise((resolve, reject) => {
            let newTicketPurchase = new Ticket(ticketPurchaseInfo);
            newTicketPurchase.delete()
                .then(model => resolve())
                .catch(err => reject(err));
        });
        return promise;
    }

    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}

module.exports = PurchaseTicketManager;