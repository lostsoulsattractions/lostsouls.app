const mysql = require("mysql");

class MySqlDatabase {
    initialize() {
        this.connectionPool = mysql.createPool({
            host: process.env.MYSQL_DB_HOST,
            user: process.env.MYSQL_USER_NAME,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DATABASE,
            timezone: "utc"
        });
        console.log("MySql connection pool created.");
    }

    formatStoredProcedureCall(storedProcName, parameters) {
        let queryString = `CALL ${storedProcName}(`;
        if (Array.isArray(parameters)) {
            // Array of parameters.
            for (let i = 0; i < parameters.length; i++) {
                queryString += (i == parameters.length - 1) ? "?)" : "?,";
            }
        } else if (parameters) {
            // Single parameter
            queryString += "?)";
        } else {
            // No parameters
            queryString += ")";
        }
        return queryString;
    }

    getConnection() {
        return new Promise((resolve, reject) => {
            this.connectionPool.getConnection((err, connection) => {
                if (err) { return reject(err); }
                resolve(connection);
            });
        });
    }

    executeStoredProcedure(storedProcName, parameters, dbConnection) {
        let promise = new Promise((resolve, reject) => {
            if (dbConnection) {
                let query = this.formatStoredProcedureCall(storedProcName, parameters);
                dbConnection.query(query, parameters, (err, resultData, fields) => {
                    // Connection is not released.
                    if (err) { return reject(err); }

                    const rows = resultData[0];
                    const resultSummaryData = resultData[1];
                    resolve(rows, resultSummaryData, fields);
                });
            } else {
                this.connectionPool.getConnection((err, connection) => {
                    if (err) { return reject(err); }
                    let query = this.formatStoredProcedureCall(storedProcName, parameters);
                    connection.query(query, parameters, (err, resultData, fields) => {
                        connection.release();
                        if (err) { return reject(err); }

                        const rows = resultData[0];
                        const resultSummaryData = resultData[1];
                        resolve(rows, resultSummaryData, fields);
                    });
                });
            }
        });

        return promise;
    }
}

module.exports = new MySqlDatabase();