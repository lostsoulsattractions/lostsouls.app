const TicketModel = require("./ticketPurchaseModel"),
    moment = require("moment-timezone"),
    validator = require("validator");

class Ticket {
    constructor(ticket) {
        this.model = new TicketModel(ticket);
    }

    save() {
        var promise = new Promise((resolve, reject) => {
            this.model.save(function (err, savedObject) {
                if (err) {
                    if (err.name === "ValidationError") {
                        reject({ "isValidationError": true, "Error": err });
                    } else {
                        reject({ "isValidationError": false, "Error": err });
                    }
                } else {
                    resolve(savedObject);
                }
            });
        });
        return promise;
    }

    delete() {
        var promise = new Promise((resolve, reject) => {
            this.model.remove(function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve({ "success": true });
                }
            });
        });
        return promise;
    }

    addUsageData(location) {
        if (!this.model.usageDetails) {
            this.model.usageDetails = [];
        }

        this.model.usageDetails.push( {
            location: location,
            ticketScanTime: Date.Now
        });

        return this.save();
    }

    clearUsageData() {
        this.model.usageDetails = [];
        return this.save();
    }

    static findById(barcodeId) {
        let promise = new Promise((resolve, reject) => {
            if (validator.isMongoId(barcodeId)) {
                TicketModel.findById(barcodeId).exec(function (err, show) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(show);
                    }
                });
            } else {
                reject("Invalid barcode ID Format: " + barcodeId);
            }
        });
        return promise;
    }

    static getLocations() {
        return TicketModel.schema.path('usageDetails.0.location').enumValues;
    }

    static getTicketTypes() {
        return TicketModel.schema.path('ticketType').enumValues;
    }
}

module.exports = Ticket;