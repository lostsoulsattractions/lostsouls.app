let Ticket;
if (process.env.DB_ENGINE && process.env.DB_ENGINE == "MongoDB") {
    Ticket = require("./ticketMongo");
} else {
    Ticket = require("./ticketMySql");
}

module.exports = Ticket;