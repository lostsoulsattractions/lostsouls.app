const TicketModel = require("./ticketPurchaseModel");
const mySqlDatabase = require("../mySqlDatabase");
const validator = require("validator");

class Ticket {
    constructor(ticket) {
        this.model = ticket;
    }

    save() {
        return new Promise((resolve, reject) => {
            let storedProcName;
            let parameters;
            let isAdd;

            if (!this.model.ticketPurchaseId) {
                // New Ticket Purchase
                isAdd = true;
                let isTestTransaction = this.model.isTestTransaction ? true : false;
                storedProcName = "spcAddTicketPurchaseInPerson";
                parameters = [this.model.barcodeId, this.model.ticketType, this.model.quantity, this.model.pricePaid, "Store", 
                              isTestTransaction, this.model.promotionCodeUsed, this.model.transactionLocation, this.model.paymentType,
                              this.model.ticketPriceType];
            } else {
                // Update and existing ticket purchase record
                isAdd = false;
                storedProcName = "spcUpdateTicketPurchase";
                parameters = [this.model.ticketPurchaseId, this.model.barcodeId, this.model.ticketType, this.model.quantity, 
                              this.model.pricePaid, this.model.emailAddress, this.model.transactionType, this.model.isTestTransaction,
                              this.model.promotionCodeUsed, this.model.transactionLocation, this.model.paymentType, this.model.ticketPriceType];
            }

            mySqlDatabase.executeStoredProcedure(storedProcName, parameters)
                .then(rows => {
                    if (isAdd && rows && rows.length) {
                        this.model.ticketPurchaseId = rows[0].TicketPurchaseID;
                        this.model.purchaseTime = rows[0].PurchaseTime
                    }

                    let additionalDbUpdatesPromises = [];

                    // Add the Ticket Purchase Payment Details records
                    this.model.paymentDetails.forEach(paymentDetail => {
                        let paymentType = paymentDetail.paymentType;
                        let transactionAmount = paymentDetail.transactionAmount;
                        additionalDbUpdatesPromises.push(mySqlDatabase.executeStoredProcedure(
                            'spcSaveTicketPurchasePaymentDetails', [this.model.ticketPurchaseId, paymentType, transactionAmount]));    
                    });

                    // Update Usage Details
                    this.usageDetailsIncluded = this.model.ticketPurchaseId && this.model.usageDetails;
                    if (this.usageDetailsIncluded) {
                        console.log("Usage Details Need to be updated");
                        this.usageDetailsIncluded = true;
                        this.model.usageDetails.forEach(usageDetail => {
                            if (!usageDetail.ticketUsageId) {
                                additionalDbUpdatesPromises.push(mySqlDatabase.executeStoredProcedure('spcAddTicketUsage', [this.model.ticketPurchaseId, usageDetail.location]));
                            } else if (usageDetail.deleted) {
                                additionalDbUpdatesPromises.push(mySqlDatabase.executeStoredProcedure("spcDeleteTicketUsage", usageDetail.ticketUsageId));
                            }
                        });
                    }

                    if (additionalDbUpdatesPromises.length) {
                        Promise.all(additionalDbUpdatesPromises)
                            .then(_ => {
                                if (this.usageDetailsIncluded)
                                {
                                    // Reload the ticket usage data from the database.
                                    this.reloadUsageData()
                                        .then(resolve(this.model))
                                        .catch(err => reject(err));
                                } else {
                                    console.log("Scenario 2: " + JSON.stringify(this.model));
                                    resolve(this.model);
                                }
                            })
                            .catch(err => reject(err));
                    } else {
                        console.log("Scenario 3: " + JSON.stringify(this.model));
                        resolve(this.model);
                    }
                })
                .catch((err) => reject(err));
        });
    }

    reloadUsageData() {
        return new Promise((resolve, reject) => {
            mySqlDatabase.executeStoredProcedure("spcGetTicketUsage", this.model.ticketPurchaseId)
                .then(rows => {
                    if (rows && rows.length) {
                        this.model.usageDetails = [];
                        rows.forEach(row => {
                            this.model.usageDetails.push({
                                ticketUsageId: row.TicketUsageID,
                                location: row.Location,
                                ticketScanTime: row.TicketScanTime,
                                isSurrogateScan: row.SurrogateScan && row.SurrogateScan[0] === 1 ? true : false
                            });
                        });
                        resolve(this.model.usageDetails);
                    }
                })
                .catch(err => reject(err));
        });
    }

    delete() {
        return new Promise((resolve, reject) => {
            mySqlDatabase.executeStoredProcedure("spcDeleteTicketPurchase", this.model.ticketPurchaseId)
                .then((_rows, resultSummaryData) => {
                    this.model = null;
                    console.log("Delete summary data:" + JSON.stringify(resultSummaryData));
                    resolve();
                })
                .catch(err => reject(err));
        });
    }

    addUsageData(location) {
        if (!this.model.usageDetails) {
            this.model.usageDetails = [];
        }

        if (!this.model.ticketPurchaseId) {
            // Ticket purchase has not yet been saved to the database.
            // Simply add the usage detail without a database save.
            this.model.usageDetails.push({
                location: location,
                ticketScanTime: new Date()
            });

            return Promise.resolve(this.model);
        }

        return new Promise((resolve, reject) => {
            let isTicketSurrogate = false;
            mySqlDatabase.executeStoredProcedure("spcAddTicketUsage", [this.model.ticketPurchaseId, location, isTicketSurrogate])
                .then(((location, rows) => {
                    let canResolve = true;
                    if (rows && rows.length) {
                        let row = (rows && rows.length) ? rows[0] : null;
                        let usageDetail = {
                            ticketUsageId: row.TicketUsageID,
                            location: location,
                            ticketScanTime: row.TicketScanTime,
                            isSurrogateScan: isTicketSurrogate
                        };
                        this.model.usageDetails.push(usageDetail);

                        // if the ticket type is both and no other scans have been made, also mark the opposite attraction as visited.
                        console.log("Debug point 1: " + this.model.ticketType);
                        if (this.model.ticketType === "TheaterAndHospital") {
                            console.log("Debug point 2: " + location);
                            let otherLocation = null;
                            if (location == "Theater") {
                                otherLocation = "Hospital";
                            } else if (location == "Hospital") {
                                otherLocation = "Theater";
                            } else {
                                console.log(`Error: Location Unexpected. Cannot mark other location ${location} scanned`);
                            }
                            console.log("Debug point 3: " + otherLocation);

                            if (otherLocation) {
                                // look for a scan at the other location
                                let otherLocationScans = this.model.usageDetails.filter(ud => ud.location == otherLocation);
                                if (!otherLocationScans || otherLocationScans.length == 0) {
                                    console.log("Debug point 4: Performing 'Other' scans");
                                    canResolve = false; // setting to false so that a second DB call can be made.  That one will perform the resolving.
                                    isTicketSurrogate = true;
                                    mySqlDatabase.executeStoredProcedure("spcAddTicketUsage", [this.model.ticketPurchaseId, otherLocation, isTicketSurrogate])
                                        .then(((secondScanLocation, secondScanRecordRows) => {
                                            console.log("Debug point 5: Saving additional ticket usage detail. " + JSON.stringify(secondScanRecordRows));
                                            if (secondScanRecordRows && secondScanRecordRows.length) {
                                                let secondScanRecordRow = (secondScanRecordRows && secondScanRecordRows.length) ? secondScanRecordRows[0] : null;
                                                let secondScanUsageDetail = {
                                                    ticketUsageId: secondScanRecordRow.TicketUsageID,
                                                    location: secondScanLocation,
                                                    ticketScanTime: secondScanRecordRow.TicketScanTime,
                                                    isSurrogateScan: isTicketSurrogate
                                                };
                                                console.log("Debug point 6: " + JSON.stringify(secondScanUsageDetail));
                                                this.model.usageDetails.push(secondScanUsageDetail);
                                            }

                                            console.log("Resolving (2)");
                                            resolve(this.model);
                                        }).bind(null, otherLocation))
                                        .catch(err => {
                                            console.log(err);
                                            // Resolve (not reject). Don't fail the promise if a failure occurs with this second scan's recording. 
                                            console.log("Resolving (3)");
                                            resolve(this.model);
                                        });
                                }
                            }
                        }
                    }
                    if (canResolve) {
                        console.log("Resolving (1)");
                        resolve(this.model);
                    }
                }).bind(null, location))
                .catch(err => reject(err));
        });
    }

    clearUsageData() {
        if (this.model && this.model.usageDetails) {
            return new Promise((resolve, reject) => {
                mySqlDatabase.executeStoredProcedure("spcDeleteTicketUsages", this.model.ticketPurchaseId)
                    .then((_rows, resultSummaryData) => {
                        this.model.usageDetails = [];
                        resolve(this.model);
                    })
                    .catch(err => reject(err));
            });
        } else {
            return Promise.resolve(this.model);
        }
    }

    static findById(barcodeId) {
        return new Promise((resolve, reject) => {
            // TODO: Validate for UUID v4 format.
            //if (!validator.isMongoId(barcodeId)) {
            //    return reject("Invalid barcode ID Format: " + barcodeId);
            //}

            mySqlDatabase.getConnection().then(connection => {
                mySqlDatabase.executeStoredProcedure("spcGetTicketPurchaseByBarcode", barcodeId, null, true)
                    .then(rows => {
                        let ticketModel = null;
                        if (rows && rows.length) {
                            let row = rows[0];
                            ticketModel = {
                                ticketPurchaseId: row.TicketPurchaseID,
                                _id: row.Barcode,
                                barcodeId: row.Barcode,
                                purchaseTime: row.PurchaseTime,
                                quantity: row.Quantity,
                                pricePaid: row.PricePaid,
                                ticketType: row.TicketType,
                                emailAddress: row.EmailAddress,
                                transactionType: row.TransactionType,
                                transactionLocation: row.TransactionLocation,
                                paymentType: row.PaymentType,
                                isTestTransaction: row.IsTestTransaction && row.IsTestTransaction[0] === 1 ? true : false,
                                promotionCodeUsed: row.PromotionCodeUsed,
                                isDeleted: row.Deleted && row.Deleted[0] === 1 ? true : false,
                                stripeChargeJson: row.StripeChargeJson,
                                stripeChargeId: row.StripeChargeId,
                                stripeRequestId: row.StripeRequestId,
                                ticketPriceType: row.TicketPriceType,
                                usageDetails: []
                            };
                        }

                        // Get ticket usage data
                        if (!ticketModel || !ticketModel.ticketPurchaseId) {
                            if (connection) { connection.release(); }
                            return resolve(ticketModel);
                        }

                        mySqlDatabase.executeStoredProcedure("spcGetTicketUsage", ticketModel.ticketPurchaseId, connection)
                            .then(rows => {
                                if (connection) { connection.release(); }
                                rows.forEach(usageRow => {
                                    ticketModel.usageDetails.push({
                                        ticketUsageId: usageRow.TicketUsageID,
                                        location: usageRow.Location,
                                        ticketScanTime: usageRow.TicketScanTime,
                                        isSurrogateScan: usageRow.SurrogateScan && usageRow.SurrogateScan[0] === 1 ? true : false
                                    });
                                });
                                resolve(ticketModel);
                            })
                            .catch(err => {
                                // Query (spcGetTicketUsage) failed.
                                if (connection) { connection.release(); }
                                reject(err);
                            });
                    })
                    .catch(err => {
                        // Query (spcGetTicketPurchaseByBarcode) failed.
                        if (connection) { connection.release(); }
                        reject(err)
                    });
            })
                .catch(err => reject(err)); // Could not obtain the connection
        });
    }

    static getLocations() {
        return TicketModel.schema.path('usageDetails.0.location').enumValues;
    }

    static getTicketTypes() {
        return TicketModel.schema.path('ticketType').enumValues;
    }
}

module.exports = Ticket;