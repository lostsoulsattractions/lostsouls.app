var mongoose = require("mongoose");
var Schema = mongoose.Schema;

const ticketTypes=['SingleAttraction', 'Theater', 'Hospital', 'TheaterAndHospital'];
const locations=['Theater', 'Hospital'];

var ticketUsageSchema = new Schema({
	location: { type: String, required: true, enum: locations },
	ticketScanTime: { type: Date, default: Date.now }
});

var ticketPurchaseSchema = new Schema({
    barcode: {type: String, required: true},
    purchaseTime: { type: Date, default: Date.now },
    quantity: { type: Number, min: 1 },
    pricePaid: { type: Number, required: true },
    ticketType: { type: String, required: true, enum: ticketTypes },
    usageDetails: [ticketUsageSchema]
});

module.exports = mongoose.model("TicketPurchase", ticketPurchaseSchema);