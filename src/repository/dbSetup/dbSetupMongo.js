(function (dbSetup) {

    const mongoose = require('mongoose'), 
          dataSeed = require("./dataSeed");
    mongoose.Promise = Promise;

    var mongoSetup = () => {
        if (process.env.DB_CONNECT_STRING) {
            mongoose.connect(process.env.DB_CONNECT_STRING, { useNewUrlParser: true })
                .then(function (db) {
                    console.log("Mongo DB Connected.");
                    if (process.env.PERFORM_DATA_SEED) {
                        console.log("Performing data seed");
                        dataSeed.init();
                    }
                });
        }
    };

    dbSetup.initialize = () => {
        mongoSetup();
    };

})(module.exports);