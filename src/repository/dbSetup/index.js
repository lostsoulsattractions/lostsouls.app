(function (dbSetup) {

    const setup = () => {
        let db = null;
        if (process.env.DB_ENGINE === "MongoDB") {
            db = require("./dbSetupMongo");
        } else if (process.env.DB_ENGINE === "MySQL") {
            db = require("./dbSetupMySql");
        } else {
            throw "Invalid or Missing DB_ENGINE environment variable";
        }

        db.initialize();
    };

    dbSetup.initialize = () => {
        setup();
    };

})(module.exports);