(function(seed){
    let Ticket = require("../../models/ticket");
    let moment = require("moment-timezone");

    const buildTicketSale = (ticketPurchaseId, id, purchaseTime, quantity, pricePaid, ticketType, usageDetails) => {

        const ticketpurchase = {
            _id: id,
            purchaseTime: purchaseTime, 
            quantity: quantity, 
            pricePaid: pricePaid, 
            ticketType: ticketType, 
            usageDetails: usageDetails
        };

        if (!process.env.DB_ENGINE || process.env.DB_ENGINE.toLowerCase() === "mysql") {
            ticketpurchase.ticketPurchaseId = ticketPurchaseId
        }

        return ticketpurchase;
    };
    
    const loadDataSeedScenarios = () => {
        let scenarios = [];

        let purchaseTime = moment().tz("America/Boise").hours(19).minute(30).seconds(0).milliseconds(0);
        var theaterAndHospitalNoUsage = buildTicketSale(
            1,
            "59adee36c876b856e0957c80", 
            moment().tz("America/Boise").hours(19).minute(30).seconds(0).milliseconds(0),
            2, 
            15.00, 
            "TheaterAndHospital",
            null);

        var hospitalNoUsage = buildTicketSale(
            2,
            "59ae2919c876b856e0957c81", 
            moment().tz("America/Boise").hours(19).minute(30).seconds(0).milliseconds(0),
            4, 
            30.00, 
            "Hospital",
            null);
        
        var theaterNoUsage = buildTicketSale(
            3,
            "59b4a1e051b3d7090c5d5283", 
            moment().tz("America/Boise").hours(19).minute(30).seconds(0).milliseconds(0),
            1, 
            7.50, 
            "Theater",
            null);

        scenarios.push(theaterAndHospitalNoUsage);
        scenarios.push(hospitalNoUsage);
        scenarios.push(theaterNoUsage);

        return scenarios;
    };

    let initialize = () => {

        var scenarios = loadDataSeedScenarios();
        scenarios.forEach(function(ticketPurchaseToSave){
            Ticket.findById(ticketPurchaseToSave._id)
                .then((data) => {
                    if (!data){
                        var ticketPurchase = new Ticket(ticketPurchaseToSave);
                        ticketPurchase.save()
                            .then(function(savedObject){
                                console.log(`Ticket Data Seed - Save Success: id: ${savedObject._id}. Ticket Type: ${savedObject.ticketType}`);
                            })
                            .catch(function(err){
                                if (err.name === "ValidatorError"){
                                    console.log(`Ticket Data Seed - Validation Error: id: ${ticketPurchaseToSave._id}. Ticket Type: ${ticketPurchaseToSave.ticketType}. Error: ${err}`);
                                } else {
                                    console.log(`Ticket Data Seed - Save Error: id: ${ticketPurchaseToSave._id}. Ticket Type: ${ticketPurchaseToSave.ticketType}. Error: ${err}`);
                                }
                            });
                    } else {
                        console.log(`Ticket Info Already in Database: ${ticketPurchaseToSave._id}`);
                    }
                })
            .catch(function(err){
                // Error looking up the data by ID, likely an issue with the database.
                console.log(`Ticket Data Seed - Find By ID Error: id: ${ticketPurchaseToSave._id}. Ticket Type: ${ticketPurchaseToSave.ticketType}. Error: ${err}`);
            });
        });
    };
    
    seed.init = () => {
        initialize();
    };

})(module.exports);