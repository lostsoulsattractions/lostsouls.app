(function(seed){

    const ticketPurchaseSeed = require("./ticketPurchaseSeed");

    seed.init = function() {
        ticketPurchaseSeed.init();
    };

    seed.reseed = () => {
        ticketPurchaseSeed.reseed();
    };

})(module.exports);

/*  Command Line Arguments for generating a new MongoDB Object ID:
    --------------------------------------------------------------

    Using Docker, issue a command against the container like this:
        $ docker exec -it lost-souls-mongo mongo

    That will open a shell prompt for MongoDB. Issue the command to generate a new ID:
        > ObjectId()
*/