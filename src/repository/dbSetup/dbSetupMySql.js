(function (dbSetup) {

    var mySqlSetup = () => {
        let database = require("../mySqlDatabase");
        database.initialize();
        // if (process.env.PERFORM_DATA_SEED || process.env.PERFORM_DATA_RESEED) {
        //     const dataSeed = require("./dataSeed");
        //     if (process.env.PERFORM_DATA_SEED) {
        //         console.log("Performing data seed");
        //         dataSeed.init();
        //     }
        // }
    };

    dbSetup.initialize = () => {
        mySqlSetup();
    };

})(module.exports);