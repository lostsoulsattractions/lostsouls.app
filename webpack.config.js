const path = require('path');

// SCSS Transformation Setup
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isDevelopment = true;

module.exports = options => {
    return {
        //context: path.join(__dirname, 'www/js'),
        entry: [
            './src/frontEnd/react/app.jsx',
            "./src/frontEnd/styles/styles.scss"
        ],
        output: {
            path: path.resolve(__dirname, 'src/frontEnd'),
            filename: 'js/bundle.js',
        },
        module: {
            rules: [
                {
                    test: /.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: [{
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                        }
                    }]
                },
                {
                    test: /\.s(a|c)ss$/,
                    exclude: /\.module.(s(a|c)ss)$/,
                    loader: [
                        //isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: isDevelopment
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            // SCSS Transformation
            new MiniCssExtractPlugin({
                filename: isDevelopment ? 'styles/[name].css' : '[name].[hash].css',
                chunkFilename: isDevelopment ? 'styles/[id].css' : '[id].[hash].css'
            })
        ],
        devtool: 'source-map',
        target: 'electron-renderer'
    }
}